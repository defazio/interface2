% NOTE PROCESSES ON EXPERIMENT AT A TIME
% master for ROI analysis
% assumes dFoF0 stack/movie is already generated 
%    (see master.m or load matlab.mat dataset)
% outline: 
%    get roi set (includes deadzone, penumbra), 
%    reprocess dF/F0 from "stack" (raw but includes crop, downsampling, etc)
%    display results
%    output to Igor? or process events here?
%% get ROIs from user
disp('PLEASE DO NOT CLOSE FIGURE! max 20 ROIs')
radius = 2;% pixels to search for half amplitude
offset = 1;% pixel separation between roi, deadzone, penumbra
hROI = click2ROI2( base0, radius, offset ); % handles contains info on ROIs
nROIs = size( hROI, 2 ); % number of ROIs

%% MOVIE TIME! for adding manual ROI
% trying to use movie for generating manual ROI over targeted cell
% trying to add existing ROIs superimposed on movie
% get coords from movie frame and add to ROI handles
% % DOES NOT WORK!
% disp('make a movie')
% movfilename = baseName; % append(baseName,".avi"); % ext is auto '.mj2'
% % V = VideoWriter(movfilename,'Archival');
% % open(V)
% posPoly = hROI(1).ROI;
% mov = movieTimeBurnIn( baseDFoF0, posPoly );
% implay( mov, 100 );
% %% writeVideo( V, mov )
% 
% manROIhandle = click2ROIman( gca );
% % writeVideo( V, mov )
% % close(V)
% % eight clicks around perimeter
% 
% disp( 'done collecting ROIs')
%% loop over ROIs to create time course data with pen subtraction
%treatStats = roi2stats3D( treat, hROI ); % uses "raw" (cropped, smoothed etc)
disp('starting roi processing')
baseStats = roi2stats3D( base, hROI ); % uses "raw" (cropped, smoothed etc)
baseStats.timing = squeeze(timing);
disp('done with ROI stats')
%%
% process ROI time course data for dF/F0
%treatStats = processROIs( treatStats );
disp('starting roi dFoF0')
baseStats = processROIs( baseStats );
disp('done with roi dFoF0')

displayROIs( baseStats );
%%
% EXPORT TO IGOR
fname = append( baseName, '_Igor');
export2igor( baseStats, fname ) % custom function for export ROI data
% save entire base stats structure
fname = append( baseName, '_baseStats');
save(fname, 'baseStats')
disp('export complete')
%%
for i=1:nROIs
displayIndyROIs( i, baseStats );
end

%% saves entire workspace, big file
filename = append( baseName, 'complete.mat');
save(filename,'-v7.3')