
function [handles,stats] = randomClick2ROI( nROI, im, stack )
% im is an image you want to use for picking ROIs

tic
%df = figure; % target for data plot
imgf = figure; % ( 'ButtonDownFcn', @figCallBackFcn );
imgo = imshow( im );
dx = size(im,1);
dy = size(im,2);

clim( 'auto' );
axis image
axis on
hold on
n = nROI; % 10;
clear handles
for i = 1 : n
    %[x,y]=rand(1:2); % ginput(1);
    x = dy * rand();
    y = dx * rand();

    if size([x,y])>0
        handles(i) = ROIfromClick(y,x, im, imgf );
        stats(i) = roi2stats3D( stack, handles(i) );
    else
        break
    end
end

toc
end
