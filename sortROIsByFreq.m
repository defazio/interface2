function sortedAnalytes = sortROIsByFreq( analytes, freq )
% analytes structure contains the following fields
%         analytes.File(iFile).ROI(iROI).peaks = pks;
%         analytes.File(iFile).ROI(iROI).locs = locs;
%         analytes.File(iFile).ROI(iROI).times = locs * dx;
%         analytes.File(iFile).ROI(iROI).width = w * dx;
%         analytes.File(iFile).ROI(iROI).prominence = p;
%         analytes.File(iFile).ROI(iROI).SD = SD;
%         analytes.File(iFile).ROI(iROI).P = P;
%         analytes.File(iFile).ROI(iROI).minHeight = minHeight;

% freq contains an array of frequencies(iFile, iROI) to sort by
[nFiles,nROIs] = size(freq);
for iFile = 1:nFiles
    vFreq = freq(iFile,:); % get the frequency vector for this file
    [ sortedvFreq, indices ] = sort( vFreq );
    for iROI = 1:nROIs
        sortedAnalytes.File(iFile).ROI(iROI) = analytes.File(iFile).ROI( indices(iROI) );   
    end
end
disp('complete')
end

