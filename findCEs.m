function [locs,pks] = findCEs( Cbest, Croll_win, Cprcthresh, CPfactor, CE_win, Fs )
    % 2023 may 4
    % Croll_win = 1; % seconds
    % Cprcthresh = 33; % percentile threshold for noise vs CE
    % CPfactor = 10;
    % CE_win = 15; % seconds, peaks within win are ignored

    Croll = Croll_win * Fs; % convert to points
    CrollStd = movstd( Cbest, Croll ); % rolling standaRDDEVIATION
    CP = prctile( CrollStd, Cprcthresh );

    %minwidth = 0.05; % time (seconds), now using Fs sampling freq
    minHeight = CPfactor * CP;
    %widthRef = 'halfprom'; %'halfheight'; %
    minPeakProm = minHeight; % Pfactor * P; % dF/F0 units
    
    [CEpks,CElocs,CEw,CEp] = findpeaks(Cbest,'MinPeakProminence',minPeakProm);

    % clean CE peaks
    npks = size( CEpks, 1);
    locs = NaN;
    pks = NaN;
    if npks>0
        % assumes first corr peak is valid
        locs = NaN( npks, 1 );
        pks = NaN( npks, 1 );
        iCE = 1;
        locs(iCE) = CElocs(1)/Fs;
        pks(iCE) = CEpks(1);
        for ipk = 2 : npks
            loc = CElocs(ipk)/Fs - locs(iCE);

            if loc > CE_win
                iCE = iCE + 1;
                locs(iCE) = CElocs(ipk) / Fs;
                pks(iCE) = CEpks(ipk);
            else
                
            end
        end
    end

end


