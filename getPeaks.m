% return dF/F0 peaks
function output = getPeaks(A)
    % A is a structure, usually named analytes
    % created by stage 3+4 analysis
    % A.file 1 x n, where n is number of files analyzed (one set of ROIs per)
    % A.file.name
    % A.file.ROI 1 x nROI
    % A.file.ROI.peaks 1 x nPeaks
    maxFiles = 20;
    maxROIs = 20;
    maxPeaks = 200;
    %pks = NaN(maxFiles,maxROIs,maxPeaks);
    nFiles = max(size(A.File));
    for i=1:nFiles % loop over files
        % each file has nROIs
        nROIs = max(size(A.File(i).ROI));
        %holder = [];
        for j=1:nROIs
            % each ROI has unique number of peaks
            local_peaks = A.File(i).ROI(j).peaks;
            nPeaks = max( size( local_peaks ) );
            peaks = A.File(i).ROI(j).peaks;
            if j==1 
                    holder = peaks;
            end
            if nPeaks>1
                %pks(i,j,1:nPeaks) = A.File(i).ROI(j).peaks(:);

                output(i).roi(j).pks = peaks;
                    if j~=1
                        holder = cat(1,holder,peaks);
                    end
            else
                disp(append("no peaks file: ",num2str(i),"; ROI: ", num2str(j)));
            end
        end
        output(i).allPeaks = holder;
    end
end