function s = TDrasterIndex( analytes, iFile, nofigure )
s=0;
% set each to something to make separate plots, its a string
spacing = 10; % units are the number of ROIs
%nFiles = size( analytes.File, 2 );
% width vs height
grey=0.75;
fontSize = 16;


    y = 0; 
    %iFile = iFile;
    y = y + spacing; %nROI;
    h=0;
if( ~nofigure ) % if the user wants a figure, nofigure = 0
    h = figure('Color',[grey,grey,grey],'Name',num2str(iFile)); 
end
    hold on
    fontsize(gca,fontSize,'points')
    set(gca,'color',[grey,grey,grey])

    y=0;

    nROI = max(size(analytes.File(iFile).ROI));
    maxEvents = 1000;
     pAll = NaN( nROI, maxEvents );
     for iROI = 1 : nROI
         p = analytes.File(iFile).ROI(iROI).prominence;
         nevents = numel( p );
         for iEvent = 1 : nevents
            pAll(iROI, iEvent)=p(iEvent);
         end
     end
     pAll = pAll(~isnan(pAll)); % KILL ALL NAN ELEMENTS!
     pmax = max( pAll, [], 'all' );

     % setup a color table for prominences
     ncolors = 256; %numel( pAll );
    % scale values to color indices
    colorFactor = ncolors / pmax;
    for iROI = 1 : nROI
        t = analytes.File(iFile).ROI(iROI).times/60; % minutes
        t50 = analytes.File(iFile).ROI(iROI).crossings/60; % minutes
        nevents = max(size(t)); % ,1);
        p = colorFactor * analytes.File(iFile).ROI(iROI).prominence;
        y = y + 1; %iROI;
        if nevents > 1 %>= 0
            yplot = zeros(nevents,1);
            yplot = yplot + y; % ROI index for y-axis
            scatter( t, yplot, [], p, "filled" );
            scatter( t50, yplot, [], p); % , "filled" );
            axis off
            h=gca;
            axis off
            %h.XAxis.Visible='off';


        end
        %exportgraphics(h,'corrgram.pdf','Append',true)
    end % loop over files
    % str=append(ROIdataStruct(iFile).name);
    % ylabel(str)
    % h.YAxis.Visible='on';
    % h.XAxis.Visible='on';
    % h.FontSize=10;
    % h.YAxis.Exponent=0;
    % ytickformat('%3.3f')
    % h.TickDir='out';
    % h.TickLength=[0.00200 0.0020];
    % str = 'time (minutes)';
    % xlabel(str);
    % linkaxes(fighandle.ax, 'x' );
    % %xlabel('time (s)','FontSize',24);
    % ylabel('ROIs'); %,'FontSize',24);
    %xlim([0 600]);
    %title('raster plot of detected events','FontSize',24);
    s = h;
end