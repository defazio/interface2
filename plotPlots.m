
% combine all data for each file
events = linearize( analytes );
binsize = 10;
xend = 600;
xstart = 0;
nbins = 1 + (xend - xstart) / binsize;
bindata = NaN(nFiles,nbins);
iFile = 9;
%for iFile=1:nFiles
    ymin = -0.02; %min(ydata,[],"all");
    ymax = 0.05; %max(ydata,[],"all");
    sh = stackplots(iFile, ROIdataStruct, ymin, ymax );
% raster
    hTDR = TDrasterIndex(analytes,iFile);
% time histo
    ts = events(iFile,:);
    [bindata(iFile,:)] = timeSeriesHistogram( ts, binsize, xstart, xend );
% movcorr
    arr = ROIdataStruct(iFile).data.pensubdf;
    xdata = ROIdataStruct(iFile).xdata;
    %rotArr = rot90(arr);
    [rr,pp] = wrapperMovCorr(arr, 50);
    meanrr = mean( rr, 2 );
    str = append('iFile: ',num2str(iFile),' ',ROIdataStruct(iFile).name);
   h = figure('Name',num2str(iFile)); 
    plot(xdata, meanrr);
    ylabel "mean_r_moveCorr"
    ylim([-0.1 1])

