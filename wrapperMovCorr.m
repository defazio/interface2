function [ rr, pp ] = wrapperMovCorr( xx, k )
% movcorr wants points not time
% no lag!

% move corr wrapper
% xx is nTimePoints x nROIs double
% k is window in points (not time units) for now
%   r = MOVCORR(x, y, k) computes r for x and y over a centered window specified
%   by k. 
%   x and y are m x 1 vectors, where m is the number of time points
%   output is r and p (significance) as a function of time and comparison (linearized)
%   output of MOVCORR: r and p are also m x 1 vectors
% output of wrapper rr, pp are m x 
% % take a breath
% gonna compare each ROI to each ROI once, over the window k, order dos
% output is lineariz
% ed, 12 13 14 1... 1n 23 24 2... 2n etc

[ nTimePoints, nROI ] = size( xx );
nC = commute( nROI ); % brute force calculation of number of comparisons
rr = NaN( nTimePoints, nC );
pp = NaN( nTimePoints, nC );

% comparisons
index = 1; % comparison index
for m = 1:nROI
    for n = (m+1):nROI
        x = xx( :, m );
        y = xx( :, n );
        % run the moving corr for this combo
        [ r, p ] = movcorr( x, y, k );
        rr( :, index ) = r;
        pp( :, index ) = p;
        index = index + 1;
    end
end

end