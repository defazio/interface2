function [ rr, l, t ] = wrapperCorrgram( xx, k, Fs, maxlag, frac_overlap )
% corrgram wants points, not time
% meets general format of C, L, T output; correlation, lag and time stamps

% xx is nTimePoints x nROIs double
% k is in points
% Fs is sampling frequency, samples per second
% maxlag is in points
% frac_overlap is the fraction of window to overlap adjacent windows

% outputs: 
% rr is correlation (nlags x nTimePoints x ncomparisons)
% l is lag in time units (nlags)

% % take a breath
% gonna compare each ROI to each ROI once, over the window k, order dos
% output is linearized, 12 13 14 1... 1n 23 24 2... 2n etc

    [ nTimePoints, nROI ] = size( xx );
    window = k; 
    noverlap = floor( frac_overlap * window ); % number of overlapping points
    nC = commute( nROI ); % brute force calculation of number of comparisons
    nLags = fix(2 * maxlag + 1);
    
    nr = fix( (nTimePoints - noverlap) / (window - noverlap) ); % number of correlations, time points
    rr = NaN( nLags, nr, nC );
    %ll = NaN( nLags );
    
    % comparisons
    index = 1; % comparison index
    for m = 1:nROI
        for n = (m+1):nROI
            x = xx( :, m );
            y = xx( :, n );
            % run the corrgram for this combo
    
            [ C, L, T ] = corrgram( x, y, maxlag, window, noverlap );
    
            rr( :, :, index ) = C;
            
            index = index + 1;
        end
    end
    l = L / Fs; % lags in time units
    t = T / Fs; % time in time units
end