% ROI selection starts here!
function [ handles ] = click2roundROI( im, imf, radius, offset ) %, stack )
% image for user to click, radius is the search radius in pixels
% returns handles for up to 20 ROI
% im is an image you want to use for picking ROIs

    n=20;
    clear handles
    for i=1:n
        % manual ROI, auto deadzone, penumbra
        h = manROI(im, radius, offset);
        if isstruct(h)
            handles(i) = h;
        else
            break
        end
    end
end
