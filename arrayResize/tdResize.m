%test resize
function downsizeStack = tdResize( stack, box )
tic
downsizeStack = arrayResize(stack, box );
toc
end