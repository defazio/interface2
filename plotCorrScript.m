Fs = 1/dx; %10; %20; % 20 hz
% Fs must account for decimate

% binsize = 10; % units?
% nbins = round( 1 + (xend - xstart) / binsize );
% bindata = NaN(nFiles,nbins);
% ymin = -0.02; %min(ydata,[],"all");
% ymax = 0.05; %max(ydata,[],"all");

    %corr params SET THESE APPROPRIATELY!!
    window_time = 2; % seconds
    maxlag_time = 0.5; % seconds
    frac_overlap = 0.95; % overlap fraction

CorrLagStore = CorrProcessing( ROIdataStruct, ...
    Fs, window_time ); %, maxlag_time, frac_overlap );
disp('completed corrProcessing...')
    % k = window_time * Fs; % corr window in points
    % maxlag = maxlag_time * Fs; % corr max lag in points
%%
set(0,'DefaultFigureWindowStyle','docked')
for iFile=1:nFiles
    xend =  max(ROIdataStruct(iFile).xdata)/60; % * 0.2 ; %seconds! 600; %
    xstart = 0;
    titleStr = append(num2str(iFile),' ',ROIdataStruct(iFile).name);
    h = figure('Name',titleStr, 'NumberTitle','off');
    tiledlayout(5,1)

    fighandle.h = h;
    fighandle.ax0 = nexttile([2,1]);

    arr = ROIdataStruct(iFile).data; %.pensubdf; % ntimepoints x nROIs
    xdata = ROIdataStruct(iFile).xdata/60; % convert to minutes
    plot(xdata, arr)
    title(titleStr)
    hold on
    nROI = size( arr, 2 );
    for iROI = 1: nROI
        % times = analytes.File(iFile).ROI(iROI).crossings/60; % minutes
        % crossings = analytes.File(iFile).ROI(iROI).crosslev;
        % scatter( times, crossings, 'o' )
        times = analytes.File(iFile).ROI(iROI).times/60; % minutes
        pks = analytes.File(iFile).ROI(iROI).peaks;
        scatter( times, pks, 'o', 'filled' )
    end
    hold off

% raster
    fighandle.ax1 = nexttile;
    nofigure =1;
    hTDR = TDrasterIndex(analytes,iFile,nofigure);

% time histo
    % ax2 = nexttile;
    % combine all data for each file
    % binsize = 5; % seconds
    % events = linearize( analytes );
    % ts = events(iFile,:);
    % [ bindata(iFile,:), hTSH ] = timeSeriesHistogram( ts, binsize, xstart, xend );

% movcorr    
    fighandle.ax3 = nexttile;

    plot(xdata, ROIdataStruct(iFile).FFA)
    str=append('full frame ave dF/F0 ');
    ylabel(str)

% plot lag
    fighandle.ax4 = nexttile;
%[ C, L, t, rrmaxmean, rrBestLagSorted ] = corrlag( arr, Fs, window_time, maxlag_time, frac_overlap );

C = CorrLagStore(iFile).C; % = C;
%L = CorrLagStore(iFile).L; % = L;
%t = CorrLagStore(iFile).t/60; % = t; % in minutes
rrmaxmean = CorrLagStore(iFile).Cmaxmean; % = rrmaxmean;
%rrBestLagSorted = CorrLagStore(iFile).Cbestlag; % = rrBestLagSorted;

    p = plot(xdata, rrmaxmean );
    xlim([xstart xend])
    str=append('correlation ', num2str(window_time), 'sec window');
    ylabel(str)
    xlabel 'time (minutes)'

    % note the array is rotated for display!!!!
    % y = 1:size(C,3); % y is index of pairs
    % hi = image(t,y,rrBestLagSorted', 'CDataMapping','scaled'); % t is time, y pair index
    % cb = colorbar;
    % ylabel( cb, 'lag (seconds)', 'Rotation', 270 )
    % ylim( cb, [ -maxlag_time, maxlag_time ] )
    % xlim([xstart xend])
    % ylabel 'sorted pairs'

    
   % linkaxes([fighandle.ax0,fighandle.ax1,fighandle.ax3], 'x' )
    linkaxes([fighandle.ax0,fighandle.ax1,fighandle.ax3,fighandle.ax4], 'x' )

    exportgraphics(h,'corrgram.pdf','Append',true)
end % loop over files
