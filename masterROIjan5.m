% master for ROI analysis
% assumes dFoF0 stack/movie is already generated 
%    (see master.m or load matlab.mat dataset)
% outline: 
%    get roi set (includes deadzone, penumbra), 
%    reprocess dF/F0 from "stack" (raw but includes crop, downsampling, etc)
%    display results
%    output to Igor? or process events here?

%% get ROIs from user
radius =4;
hROI = click2ROI( meanDFoF0, radius ); % handles contains info on ROIs

%% loop over ROIs to create time course data with pen subtraction
stack = base;
stats = roi2stats3D( stack, hROI ); % uses "raw" (cropped, smoothed etc)

%% process ROI time course data for dF/F0
stats = processROIs( stats );
%% display ROIs for analysis
set(0, 'DefaultAxesFontSize', 18);
iFile=1;
name = ROIdataStruct(iFile).name;
pensubdf = ROIdataStruct(iFile).data;
ROIstack = cell2mat(struct2cell(pensubdf));
iROI = 1;
smooth = 1; % all in points not time
derPreSmooth = 1;
derPostSmooth = 1;
ROIstack = cell2mat(struct2cell(pensubdf)); %pensubdf;
%dx=0.05;
[ smth, SD, der, SDder ] = prepdispROIstack(iROI,ROIstack,dx,smooth,derPreSmooth,derPostSmooth);
[hROIplot, ld, xdata] = plotROI( smth, SD, der, SDder );
%%
set(0, 'DefaultAxesFontSize', 18);
smooth = 11;
derPreSmooth = 1;
derPostSmooth = 1;
Fs = 10; % Hz, sampling frequency %% seconds %/60seconds = minutes5
%Pfactor = 7; % P factor factor of 10th percentile rolling SD for prominence threshold
analytes = analyzeROIs( ROIdataStruct, dx, smooth, derPreSmooth, derPostSmooth, Fs, Pfactor);
%%
set(0, 'DefaultAxesFontSize', 18);
hA = plotAnalytes( analytes );
%% kill figure
delete(hROIplot)
clear hROIplot
close(hROIplot)
