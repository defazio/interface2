function sh = stackplots( iFile, ROIdataStruct, ymin, ymax )
nFiles = size( ROIdataStruct, 2 );
%for iFile = 1 : nFiles     
    nROI = size( ROIdataStruct(iFile).data.pensubdf, 2 );
    name = ROIdataStruct(iFile).name;
    xdata = ROIdataStruct(iFile).xdata;
    ydata = ROIdataStruct(iFile).data.pensubdf;
    str = append( num2str(iFile),' - ',name);
    h = figure( 'Name', str );
    sh = stackedplot( xdata, ydata);
%     ymin = min(ydata,[],"all");
%     ymax = max(ydata,[],"all");
%     dy = ymax - ymin;
%     offset = 0.02; % 1% offset for graphs
%     ymin = ymin - offset*dy;
%     ymax = 0.05 * ymax; %  + offset*dy;
%     ymin = -0.02;
%     ymax = 0.05;
    for iROI = 1:nROI
        sh.AxesProperties(iROI).YLimits = [ymin ymax];
    end
%end
end