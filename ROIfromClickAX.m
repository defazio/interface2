function h = ROIfromClick3(x,y,imMat, ifig, r, offset ) %, dfig)
% x, y position of click, imMat is stack, ifig is the figure,
% r is radius

% creates ROI, deadzone, and penumbra coordinates
% returns in h: 3 sets of coords, polygon handles
%r = 20;
deadzoneOffset = offset;
penumbraOffset = offset;

[ xxx, vp ] = ringCoordFromXY2( imMat, x, y, r );
h.ROI = xxx; % coordinates of ROI, 8x2
% flip x and y
xxx(:,1)=h.ROI(:,2);
xxx(:,2)=h.ROI(:,1);
h.vp = vp; % stores the profiles used to auto generate ROI

figure(ifig)

factor = sqrt(0.5); % conversion from hypoteneuse to catheti
%dilation =[0,-1; factor,-factor; 1,0; factor,factor; 0,1; -factor,factor; -1, 0; -factor,-factor];
        % XXX counter clockwise from 12 o'clock
        % now couter clockwise from 9 o'clock!
        % matlab origin is top left
dilation =[-1,0; -factor,+factor; 0,1; factor,factor; 1,0; factor,-factor; 0, -1; -factor,-factor];

msize = 1; 
lsize = 1;

h.handles.ROI = drawpolygon(gca,'Position', xxx, 'FaceAlpha', 0, 'LineWidth', lsize, 'MarkerSize', msize );

deadzone = xxx + deadzoneOffset * dilation;
h.deadzone = deadzone;
h.handles.deadzone = drawpolygon(gca,'Position', deadzone, 'LineWidth', lsize, 'MarkerSize', msize, 'Color', [1,1,1], FaceAlpha=0 );

penumbra = deadzone + penumbraOffset * dilation;
h.penumbra = penumbra;
h.handles.penumbra = drawpolygon(gca,'Position', penumbra, 'LineWidth', lsize, 'MarkerSize', msize,  'Color', [1,0,1], FaceAlpha=0 );

end