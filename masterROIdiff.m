% master for ROI analysis
% assumes dFoF0 stack/movie is already generated 
%    (see master.m or load matlab.mat dataset)
% outline: 
%    get roi set (includes deadzone, penumbra), 
%    reprocess dF/F0 from "stack" (raw but includes crop, downsampling, etc)
%    display results
%    output to Igor? or process events here?

%% get ROIs from user
disp('PLEASE DO NOT CLOSE FIGURE! max 20 ROIs')
radius = 4;
hROI = click2ROI( maxDFoF0, radius ); % handles contains info on ROIs
nROIs = size( hROI, 2 ); % number of ROIs

% loop over ROIs to create time course data with pen subtraction
%treatStats = roi2stats3D( treat, hROI ); % uses "raw" (cropped, smoothed etc)
baseStats = roi2stats3D( base, hROI ); % uses "raw" (cropped, smoothed etc)

% process ROI time course data for dF/F0
%treatStats = processROIs( treatStats );
baseStats = processROIs( baseStats );

displayROIs( baseStats );

% EXPORT TO IGOR
fname = append( baseName, '_Igor');
export2igor( baseStats, fname )
%%
for i=1:nROIs
displayIndyROIs( i, baseStats );
end

%%
filename = append( baseName, 'complete.mat');
save(filename,'-v7.3')