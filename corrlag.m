function [ C, ll, t, rrmaxmean, rrBestLagSorted ] = corrlag( arr, Fs, window_time, maxlag_time, frac_overlap )
% handles all the calculations for moving correlation with lag
% wrapper for the wrapper for "corrgram.m" Norwan!

    %corr params
    k = fix(window_time * Fs); % corr window in points
    maxlag = fix(maxlag_time * Fs); % corr max lag in points

%    [ rr, pp ] = wrapperMovCorr( arr, k ); %, Fs, maxlag, frac_overlap );
     % MovCorr doesn't test lag
     
% for corrgram, k and maxlag are in points!
    [ C, ll, t ] = wrapperCorrgram( arr, k, Fs, maxlag, frac_overlap );

%    [ C, lag_time, t ] = wrapperMIgram( arr, k, Fs, maxlag, frac_overlap );
%    [ rr, ll, t ] = wrapperTWX( arr, k, Fs, maxlag, frac_overlap );

    [ rrmax, rrmaxi ] = max( C, [], 1 ); % get the max 'best' correlation over lag
    rrmaxi = squeeze( rrmaxi );
    rrmax = squeeze( rrmax );
    % take the mean over all the comparisons
    rrmaxmean = mean( rrmax, 2 ); % this is "overall correlation" as a function of time
    % mean is taken over all pairs

    rrbestlag = ( rrmaxi - k )/Fs; % convert index to lag time
    meansOfCols = mean( rrbestlag, 1 );
    % reorder pairs based on lowest mean lag over time (not absolute, neg lag is 'good')
    [ ~ , sortOrder ] = sort( meansOfCols );
    rrBestLagSorted = rrbestlag( :, sortOrder );

end
