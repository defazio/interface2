function crosscorrelation( data )
% data contains info for each ROI, first index; 2nd index is time series

numROIs = size(data, 1); % get the number of ROIs
corrMat = zeros(numROIs); % initialize the correlation matrix with zeros

% loop over all pairs of ROIs and calculate their cross-correlation
for i = 1:numROIs
    stream1 = data(i, 1:end); % extract the event times for ROI i
    stream1( isnan(stream1) ) = [];
    n1 = size(stream1,2);
    for j = i+1:numROIs
        stream2 = data(j, 1:end); % extract the event times for ROI j
        stream2( isnan(stream2) ) = [];
        n2 = size(stream2,2);
        [corr, lags] = xcorr(stream1, stream2, 'coeff'); %'none' ); % 'coeff'); % calculate the cross-correlation
        max_corr = max(corr) / (n1*n2); % get the maximum correlation value
        %lags(lags==0)=[];
        %min_lag = min(abs(lags));
        corrMat(i,j) = max_corr; % store the max correlation value in the matrix
        corrMat(j,i) = max_corr; % store the max correlation value in the matrix (symmetric)
    end
end

% display the correlation matrix
%disp(corrMat);

% display correlation matrix
figure;
imagesc(corrMat);
colorbar;
title('Cross-Correlation Matrix');
xlabel('ROI');
ylabel('ROI');