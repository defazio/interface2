function [ trace, der ] = dispROIstack( iROI, ROIstack, smooth, derPreSmooth, derPostSmooth)
trace = ROIstack(:,iROI); %ROIstats(iROI).dFoF0.ROI; %pensub;
%n = size( ROIstats, 2 ); % number of ROIs

% load the dFoF0 data into the ROIstats structure and return
    figure
    linkdata on
    hold on
%for i=1:n % loop over ROIs to store the processed data
    %ROIstats(i).dFoF0.ROI = squeeze( dFoF0( 1, i, : ) );

    smth = boxcar( trace , smooth );
    plot( smth ); %= squeeze( dFoF0( 2, i, : ) );
    SD = std( trace );
    yline(0.5*SD);
    yline(0.25*SD);
    %plot( 2*movstd(ROIstats(iROI).dFoF0.pensub,200 ))
    yyaxis right
    dx = 0.1; % sec
    prewin = derPreSmooth; % boxcar smooth window
    postwin = derPostSmooth;
    der = diffSmooth( trace, dx, prewin, postwin );
    SDder = std( der );
    plot( der );
    yline(SDder);

%end

end