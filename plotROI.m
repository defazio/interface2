function [h] = plotROI( smth, SD, der, SDder, xdata )
figureName = 'autoupdate plot';
h = findall(groot,'type','figure','name',figureName);
if isempty(h)
   % Figure doesnt exist
   h = figure( 'Name', figureName, 'position', [150,0,1000,650] );
set(gcf,'Visible','on')
   l = linkdata(h);
   h1 = subplot( 2, 1, 1, 'Parent', h );
   % make fake x data
   n = size( smth, 1 );
   %dx=0.05; % seconds
   %xdata = 0:dx:dx*(n-1);
   p1 = plot( xdata, smth ); 
   p1.XDataSource = 'xdata';
   p1.YDataSource = 'smth';
   %yline( 0.5*SD );
   %yline( 0.25*SD );
   axis tight
   linkdata on
   xlabel('time (s)' ); %,'FontSize',16)
   ylabel('dF/F0' ); %,'FontSize',16)
   title('relative calcium level' ); %,'FontSize',16)
    ax1 = gca;
   h2 = subplot( 2, 1, 2, 'Parent', h );
   p2 = plot( xdata, der );
   p2.XDataSource = 'xdata';
   p2.YDataSource = 'der';
   ax2 = gca;
   %yline( SDder );
   linkdata on
   %linkdata(h,'on')
   %l
   xlabel('time (s)' ); %,'FontSize',16)
ylabel('deriative dF/F0' ); %,'FontSize',16)
title('derivative' ); %,'FontSize',16)
axis tight
linkaxes([ax1,ax2],'x');
else
   % Figure exists
   %disp('plotROI: muchas problemas...')
end

end