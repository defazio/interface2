function [ events ] = linearize( analytes )
% take all the events from all the rois and make a single row/column for each file
    nFiles = size( analytes.File, 2 );
    maxROI = 20;
    maxEvents = 200;
    events = NaN(nFiles, maxROI*maxEvents);

    for iFile = 1:nFiles 
        nROI = size(analytes.File(iFile).ROI,2);
        jEvent = 1;
        for iROI = 1 : nROI
            ts = analytes.File(iFile).ROI(iROI).times;
           % times = sort( times );
            nEvents = max(size( ts ));
            %disp([iFile, iROI, nEvents])
            if nEvents > 1
                for iEvent = 1:nEvents
                    events( iFile, jEvent ) =  ts( iEvent );
                    jEvent = jEvent + 1;
                end % loop over events
            end % end if there are events
        end % loop over ROIs
        
    end % loop over files
    events = sort( events, 2 );
end
