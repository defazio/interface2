function y = boxcar( x, windowSize )

b = (1/windowSize)*ones(1,windowSize);
a = 1;
%Find the moving average of the data and plot it against the original data.

y = filter(b,a,x);

end

