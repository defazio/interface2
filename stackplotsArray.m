function sh = stackplotsArray( xdata, arr, ymin, ymax )
 
    nROI = size( arr, 2 );
    sh = stackedplot( xdata, arr );
    for iROI = 1:nROI
        sh.AxesProperties(iROI).YLimits = [ymin ymax];
    end

end