Fs = 1/0.05; %10; %20; % 20 hz
% Fs must account for decimate

set(0,'DefaultFigureWindowStyle','docked')

% Load CorrLagStore structure
load('CLS_20230608b_01.mat', 'CorrLagStore');

% Load ROIdataStruct structure
load('RDS_20230608b_01.mat', 'ROIdataStruct');
nFiles = numel(ROIdataStruct);
for iFile=1:nFiles
    xend =  max(ROIdataStruct(iFile).xdata)/60; % * 0.2 ; %seconds! 600; %
    xstart = 0;
    titleStr = append(num2str(iFile),' ',ROIdataStruct(iFile).name);
    h = figure('Name',titleStr, 'NumberTitle','off');


binsize = 10; % units?
nbins = round( 1 + (xend - xstart) / binsize );
bindata = NaN(nFiles,nbins);
% ymin = -0.02; %min(ydata,[],"all");
% ymax = 0.05; %max(ydata,[],"all");

% corr params SET THESE APPROPRIATELY!!
window_time = 2; % seconds
maxlag_time = 0.5; % seconds
frac_overlap = 0.95; % overlap fraction


disp('completed corrProcessing...')
% k = window_time * Fs; % corr window in points
% maxlag = maxlag_time * Fs; % corr max lag in points
%%
    tiledlayout(4,1)

    fighandle.h = h;
    fighandle.ax0 = nexttile([2,1]);

    arr = ROIdataStruct(iFile).data; %.pensubdf; % ntimepoints x nROIs
    xdata = ROIdataStruct(iFile).xdata/60; % convert to minutes
    plot(xdata, arr)
    title(titleStr)
    hold on
    nROI = size( arr, 2 );

    % plot lag
    fighandle.ax4 = nexttile;
C = CorrLagStore(iFile).C; % = C;
%L = CorrLagStore(iFile).L; % = L;
%t = CorrLagStore(iFile).t/60; % = t; % in minutes
rrmaxmean = CorrLagStore(iFile).Cmaxmean; % = rrmaxmean;
%rrBestLagSorted = CorrLagStore(iFile).Cbestlag; % = rrBestLagSorted;

    p = plot(xdata, rrmaxmean );
    xlim([xstart xend])
    str=append('correlation ', num2str(window_time), 'sec window');
    ylabel(str)
    xlabel 'time (minutes)'

 % Calculate p-values
    fighandle.ax5 = nexttile([1, 1]);
    r_values = CorrLagStore(iFile).C; % Correlation coefficients
    nPairs = size(r_values, 2);
    p_values = zeros(size(r_values));
    
    for i = 1:size(r_values, 1)
        r = r_values(i, :);
        t_stat = r .* sqrt((nPairs - 2) ./ (1 - r.^2));
        p_values(i, :) = 2 * (1 - tcdf(abs(t_stat), nPairs - 2));
    end

    plot(xdata, p_values);
    xlim([xstart xend]);
    str = append('P-values for ', num2str(window_time), ' sec window');
    ylabel('p-value');
    xlabel('Time (minutes)');

    % Link x-axes
    linkaxes([fighandle.ax0, fighandle.ax4, fighandle.ax5], 'x')

    % Save figure
    exportgraphics(h, 'corrgram.pdf', 'Append', true)
end % Loop over files

