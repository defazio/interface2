%resize the time dimension of an image stack
function resize = substackTime( stack, fstart, fend )
% x-y-nframes stack
% f number of frames to average, 
% output will be x by y by nframes/f
height = size( stack, 1);
width = size( stack, 2);
nframes = fend - fstart + 1; % size( stack, 3);

resize = zeros( height, width, nframes, class(stack));
disp('starting time resize...');
tic
%for f = fstart:fend
    resize( :, :, : ) = stack( :, :, fstart:fend);
%end
toc
end