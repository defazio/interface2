function h = manROI( im, r, offset )
% assumes open figure
% h is struct array of ROI handles
% get a click
    [x,y]=ginput(1);
    if size([x,y])>0
        % make octogon around center point
        factor = sqrt(0.5); % conversion from hypoteneuse to catheti
        diag =[-1,0; -factor,factor; 0,1; factor,factor; 1,0; factor,-factor; 0, -1; -factor,-factor];
        %counter clockwise from 9 o'clock
        % matlab origin is top left

        [ ypnts, xpnts ] = size(im);
        Xmin = x - r;
        if(Xmin < 1)
            Xmin = 1;
        end
        Xmax = x + r;
        if(Xmax > xpnts)
            Xmax = xpnts;
        end
        Ymin = y - r;
        if(Ymin < 1)
            Ymin = 1;
        end
        Ymax = y + r;
        if(Ymax > ypnts)
            Ymax = ypnts;
        end
        eightPairs = zeros(8,2);
        % vertical, v
        eightPairs(1,1) = Xmin;
        eightPairs(5,1) = Xmax;
        eightPairs(1,2) = y;
        eightPairs(5,2) = y;
        
        % horizontal, h
        eightPairs(3,1) = x;
        eightPairs(7,1) = x;
        eightPairs(3,2) = Ymax;
        eightPairs(7,2) = Ymin;
        
        % diagonal
        lxy = sqrt( (r^2) / 2 ); 
        % +45 degrees, p
        eightPairs(2,1) = x - lxy;
        eightPairs(6,1) = x + lxy;
        eightPairs(2,2) = y + lxy;
        eightPairs(6,2) = y - lxy;
        
        % -45 degrees, m
        eightPairs(4,1) = x + lxy;
        eightPairs(8,1) = x - lxy;
        eightPairs(4,2) = y + lxy;
        eightPairs(8,2) = y - lxy;
        
        % make dead zone and penumbra, store polygons
        msize = 1; 
        lsize = 1;
        
        %h.ROI = eightPairs;
     
        deadzoneOffset = offset;
        penumbraOffset = offset;

        % expand ROI to generate deadzone
        deadzone = eightPairs + deadzoneOffset * diag;
        %h.deadzone = deadzone;
        
        % expand deadzone to make penumbra
        penumbra = deadzone + penumbraOffset * diag;
        %h.penumbra = penumbra;

        h.penumbra = penumbra;
        h.deadzone = deadzone;
        h.ROI = eightPairs;

    % draw the polygons, ROI on top
        h.handles.penumbra = drawpolygon(gca,'Position', penumbra, 'LineWidth', lsize, 'MarkerSize', msize,  'Color', [1,0,1], FaceAlpha=0 );
        h.handles.deadzone = drawpolygon(gca,'Position', deadzone, 'LineWidth', lsize, 'MarkerSize', msize, 'Color', [1,1,1], FaceAlpha=0 );
        h.handles.ROI = drawpolygon(gca,'Position', eightPairs, 'FaceAlpha', 0, 'LineWidth', lsize, 'MarkerSize', msize );
   
    else
        h = 'empty';
    end
end