% ancient, predates correlation processing
function [ centered ] =plotOrder( times , centertimes, CEstore )
% times is an array, each row is a coordinated event, CE
% each column is the time of a "test" event in thate coordinated event
% each row has the "test" event time first,
% followed by at least one event from another ROI within maxDelta 

% get the starts and ends for each CE
nCE = size( times, 1 );
starts = min( times, [], 2);
ends = max( times, [], 2 );
maxEvents = size(times, 2);
ROIs = NaN(maxEvents,1);
centered = NaN( nCE, maxEvents );
% first edge is always zero
% first count is always zero

for k = 1 : nCE
    for l = 1 : maxEvents
        centered(k,l) = times(k,l) - centertimes( k ); % assumes both sorted appropriately
        ROIs(k,l) = CEstore( k, l, 2); % position 2 contains ROI index for each event
    end
    %widths(k) = ends(k) - starts(k);
    %counts(k) = sum( ~isnan( times( k, : ) ) );
end
figure
hold on
for k=1:nCE
    xdata = centered(k,:);
  ydata = ROIs(k,:);
    scatter( xdata, ydata, 'filled' );% ,']BarWidth',widths(k));
end
xlabel('time (sec, relative to center)' ); %,'FontSize',16)
ylabel( 'ROI index' ); %,'FontSize',16)
title(append('brute force order') ); %,'FontSize',16)
hold off
end