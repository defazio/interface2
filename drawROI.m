function h2 = drawROI( h, axe )
% h is a handle structure containing AT LEAST .ROI, .deadzone and .penumbra
% these contain coordinates of the actual ROI, deadzone and penumbra 
% (NOT THE POLYGON HANDLES!)
% axe is axes handle

    msize = 1;
    lsize = 1;
    nROIs = size(h,2);
    for i = 1:nROIs
        h2(i).ROI = h(i).ROI; % roi;
        h2(i).deadzone = h(i).deadzone; %dead;
        h2(i).penumbra = h(i).penumbra; %pen;
        h2(i).handles.ROI = drawpolygon(axe,'Position', h2(i).ROI, 'FaceAlpha', 0, 'LineWidth', lsize, 'MarkerSize', msize );
        h2(i).handles.deadzone = drawpolygon(axe,'Position', h2(i).deadzone, 'LineWidth', lsize, 'MarkerSize', msize, 'Color', [1,1,1], FaceAlpha=0 );
        h2(i).handles.penumbra = drawpolygon(axe,'Position', h2(i).penumbra, 'LineWidth', lsize, 'MarkerSize', msize,  'Color', [1,0,1], FaceAlpha=0 );
    end
end