% Load the data from a file if needed
data = load('ROIdata.mat');
ROIdata = data.arr; % get numeric data from the struct field

% Set the window size and step size
windowSize = 3; % or any other value
stepSize = 10; % or any other value

% Compute the rolling correlation between each pair of ROIs
numROIs = size(ROIdata, 2);
if size(ROIdata, 1) < windowSize
    error('Window size is too large for the size of ROIdata.');
end
numWindows = floor((size(ROIdata, 1) - windowSize) / stepSize) + 1;
corrMat = NaN(numROIs, numROIs, numWindows);
for t = 1:numWindows
    windowData = ROIdata((t-1)*stepSize+1:(t-1)*stepSize+windowSize, :);
    corrMat(:, :, t) = corrcoef(windowData);
end

% Print some diagnostic information
disp(['Dimensions of corrMat: ', num2str(size(corrMat))]);
disp(['Number of windows: ', num2str(numWindows)]);

% Visualize the rolling correlation matrix for a single window
figure;
imagesc(corrMat(:, :, 1));
colorbar;
title('Rolling correlation matrix for window starting at timepoint 1');


