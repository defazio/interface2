function displayROIstats(ROIstats)

for i=1:size(ROIstats,2)
    figure
    hold on
    plot(ROIstats(i).ROI.meanZ)
    plot(ROIstats(i).deadzone.meanZ)
    plot(ROIstats(i).penumbra.meanZ)
end
figure
hold on
for i=1:size(ROIstats,2)
    plot(ROIstats(i).dFoF0.ROI)
    %plot(ROIstats(i).dFoF0.pensub)
end
figure
hold on
for i=1:size(ROIstats,2)
    %plot(ROIstats(i).dFoF0.ROI)
    plot(ROIstats(i).dFoF0.pensub)
    display(num2str(std(ROIstats(i).dFoF0.pensub)));
end
end