function compStack(st1, st2, nave)
% st1 st2 are image stacks (x,y,t)
% nave is the number of frames to average
    make_avi = true;
    if make_avi
        vidObj = VideoWriter('compStack.avi','Motion JPEG AVI');
        set(vidObj,'FrameRate',30);
        vidObj.Quality=100;
        open(vidObj);
    end
    fig = figure;
    set(fig,'WindowStyle','normal')
    nFrames = size(st1,3);
    for t = 1:nave:nFrames
        tend=t+nave-1;
        if(tend>nFrames)
            tend=nFrames;
        end
        im1 = mean(st1(:,:,t:tend),3); % average nave frames
        im2 = mean(st2(:,:,t:tend),3);
        C = imfuse(im1,im2,'falsecolor','Scaling','independent','ColorChannels', [2 1 2]);
        imshow(C,'InitialMagnification',200)
        title(sprintf('Frame %i out of %i',t,size(st1,3)),'fontweight','bold','fontsize',14); 
        drawnow;
        currFrame = getframe(fig);
        writeVideo(vidObj,currFrame);    
    end
    if make_avi
        close(vidObj);
    end
end