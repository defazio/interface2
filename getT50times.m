function analytes = getT50times( analytes )
% analytes is analytes.file(ifile).ROI(iROI)    .peaks(iEvent)
%                                               .locs
%                                               .times
%                                               .width
%                                               .prominence
% this function will add analyte
% the time the event crosses 50% of the peak (risetime)
% calculated from .times and .width
% T50time = .times - 0.5 * .width
nFiles = size(analytes.File,2);
for iFile = 1 : nFiles
    nROIs = size(analytes.File(iFile).ROI,2);
    for iROI = 1 : nROIs
        times = analytes.File(iFile).ROI(iROI).times; % array of event times
        width = analytes.File(iFile).ROI(iROI).width; % array of event widths
        % T50times = times - 0.5 * width;
        % doesn't work because width is assymetrical
        
        
        analytes.File(iFile).ROI(iROI).T50times = T50times;
    end
end