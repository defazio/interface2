% process migram test

%Plot results:
%-------------
figure;

tiledlayout(5,1)
nexttile

plot( xdata, input1 )
ylabel "input1"
nexttile

plot( xdata, input2 )
ylabel "input2"
nexttile

meanC = mean( C, 1 );
maxC = max( C, [], 1 );
%meanXCL = mean( xcl, 2 );

yyaxis left
plot( T , meanC )
ylabel "migramMeanC"
yyaxis right
 plot( T, maxC)
 ylabel "migramMaxC"
xlim([0 600])
nexttile 

pcolor( T, L, C )
xlim([0 600])
shading flat
nexttile

% pcolor(twin,lag_time,xcl')
% xlim([0 600])
% shading flat

figure
hold on
yyaxis left
ylabel "corrgram"
plot( T, meanC )
yyaxis right
ylabel "timeWindowXcorr"
plot( twin, meanXCL )

figure
hold on
yyaxis left
plot( xdata, input1, xdata, input2)
yyaxis right
plot( T, meanC )
xlim([0 600])

xs = 150; % time in seconds
xe = 170;
nsmooth = 3;
thresh = 0.03;
superimposePlots2( input1, input2, xdata, xs, xe, nsmooth, thresh );
hold on
yyaxis right
plot( T, meanC )