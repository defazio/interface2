% move full image stack to array
series1 = data{1,1}; % move cell containing frame cells
height = size( series1{1,1}, 1 );
width = size( series1{1,1}, 2 );
frames = size( data{1,1}, 1);
stack1 = zeros(height, width, frames, 'uint16');
tic
for p = 1 : frames
     series1c = series1{p,1}; % moves a frame to temp variable
     stack1(:,:,p) = series1c; % moves temp variable containing frame to h5 structure

end
toc