localpath = '/gcamp live/arc/di';
S_di = tiffDirHisto(localpath);
%%
localpath = '/gcamp live/arc/intact male';
S_im = tiffDirHisto(localpath);
%%
localpath = '/gcamp live/arc/orx';
S_orx = tiffDirHisto(localpath);
%%
localpath = '/gcamp live/arc/ovx';
S_ovx = tiffDirHisto(localpath);
%%
localpath = '/gcamp live/arc/ovxe';
S_ovxe = tiffDirHisto(localpath);
%%
localpath = '/gcamp live/avpv/di';
S_avpv_di = tiffDirHisto(localpath);
%%
localpath = '/gcamp live/avpv/intact male';
S_avpv_im = tiffDirHisto(localpath);
%%
%%localpath = '/gcamp live/avpv/orx';
%%S_avpv_orx = tiffDirHisto(localpath);
%%
localpath = '/gcamp live/avpv/ovx';
S_avpv_ovx = tiffDirHisto(localpath);
%%
localpath = '/gcamp live/avpv/ovxe';
S_avpv_ovxe = tiffDirHisto(localpath);
%%
S = S_avpv_ovxe;
nImages = size(S.Image,2);
nROIs = 20;
S.ROImeans = NaN(nROIs,nImages);
for i=1:nImages
    im = S.Image(i).img;
    if size(im)>0
        hin = S.Image(i).hROI;
        S.Image(i).ROIstats = roi2statsImage( im, hin );
       % v = S.Image(i).ROIstats.ROI(:).value;
       nR = S.Image(i).ROIstats.nROIs;
       for j=1:nR
            S.ROImeans(j,i) = S.Image(i).ROIstats.values(j);
            S.pens(j,i) = S.Image(i).ROIstats.pens(j);
       end
    end
end
S_avpv_ovxe= S;
