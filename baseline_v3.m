    t=imread(mytiff);
    % imshow(t);
    % clim('auto');
% figure
%     histogram(t);

    %% get ROIs from user
disp('PLEASE DO NOT CLOSE FIGURE! max 20 ROIs')
radius = 8;% pixels to search for half amplitude, use 2 pixel if spatial resize
offset = 2;% pixel separation between roi, deadzone, penumbra
% click2ROI2 is fixed size ROIs, independent of x-y profile 
%hROI = click2ROI2( maxDFoF0, radius, offset ); % handles contains info on ROIs
hROI = click2ROI( t, radius, offset ); % handles contains info on ROIs
nROIs = size( hROI, 2 ); % number of ROIs
