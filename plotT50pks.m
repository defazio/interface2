% combine all data for each file
events = linearize( analytes );

binsize = 5; % seconds
xend = 600; % max(xdata);
xstart = 0;
nbins = 1 + (xend - xstart) / binsize;
bindata = NaN(nFiles,nbins);
ymin = -0.02; %min(ydata,[],"all");
ymax = 0.05; %max(ydata,[],"all");

set(0,'DefaultFigureWindowStyle','docked')
iFile = 11;
%for iFile=1:nFiles

    str = append(num2str(iFile),' ',ROIdataStruct(iFile).name);
    figure('Name',str, 'NumberTitle','off')
    %tiledlayout(4,1)

    ax0 = nexttile([2,1]);
    arr = ROIdataStruct(iFile).data.pensubdf; % ntimepoints x nROIs
    xdata = ROIdataStruct(iFile).xdata;
    plot(xdata, arr)
    hold on
    nROI = size( arr, 2 );
    for iROI = 1: nROI
        times = analytes.File(iFile).ROI(iROI).crossings;
        crossings = analytes.File(iFile).ROI(iROI).crosslev;
        scatter( times, crossings, 'o' )
        times = analytes.File(iFile).ROI(iROI).times;
        pks = analytes.File(iFile).ROI(iROI).peaks;
        scatter( times, pks, 'o', 'filled' )
    end
    hold off