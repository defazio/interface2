function [CEstore, times, timeSorted, centerTime, centerTimeSorted] = coordination( iFile, analytes, maxDelta )
% analytes is the structure containing timing information
% max Delta is the maximum interval for coordination (events must be within
% this time frame before or after)
% CE, coordinated events structure? need to store t, ROI, and event indices
%for iFile = 1 : nFiles
%iFile = 1;
    nROI = max(size(analytes.File(iFile).ROI));
    % get the total number of events
    eventTotal = 0;
    for iROI = 1 : nROI
        timesBase = analytes.File(iFile).ROI(iROI).times;
        nevents = size(timesBase,1);
        eventTotal = eventTotal + nevents;
    end
    disp(append('event total: ',num2str(eventTotal)))

    if eventTotal > 1
        iCE = 0;
        centerTime = 0;
        eventIncrement = 0;
        maxEvents = 100;
        maxCEs = 100;
        CEstore = NaN(maxCEs, maxEvents, 3); % room to store time, ROI#, ROIevent#
        iFlag=zeros(nROI,maxEvents);
        for iROI = 1 : nROI
            timesBase = analytes.File(iFile).ROI(iROI).times;
            nevents = size(timesBase,1);
            for ie = 1 : nevents
                baseEventTime = timesBase(ie);
                for iROItest = iROI+1 : nROI
                    timesTest = analytes.File(iFile).ROI(iROItest).times;
                    neventsTest = size(timesTest,1);
                    for ieTest = 1 : neventsTest

                        if iFlag(iROItest,ieTest)~=1 % should prevent events from being in more than one CE
                            testEventTime = timesTest(ieTest);

                            % is the event with in the interval of the base event
                            deltaT = abs( baseEventTime - testEventTime );

                            if deltaT < maxDelta 
                                % locate the nearest CE, but how?
                                % return elements of CE(iCE).centerTime that are
                                % within maxDelta
                                if iCE == 0 % set up the first coordinated event
                                    iCE = 1;
                                    % store the founding/base event
                                    centerTime(iCE) = baseEventTime; % founding event
                                    eventIncrement(iCE) = 1;
                                    CEstore( iCE, eventIncrement(iCE), : ) = [ baseEventTime, iROI, ie ];

                                    % alternate storage method for sorting
                                    times( iCE, eventIncrement(iCE) ) = baseEventTime;% this needs to be fancier, centered on mean of times 

                                    % store the test event
                                    eventIncrement(iCE) = eventIncrement(iCE) + 1;
                                    CEstore( iCE, eventIncrement(iCE), :  ) = [ testEventTime, iROItest, ieTest ];
                                    % separate arrays for sorting
                                    times( iCE, eventIncrement(iCE) ) = testEventTime;% this needs to be fancier, centered on mean of times 

                                else % find the nearest coordinated event
                                    
                                    CEdeltaT = abs( centerTime - testEventTime );
                                    I = find( CEdeltaT < maxDelta, 1 );

                                    % if there is no closest CE, make a new one
                                    % if isempty(closeCEs) %|| ~( iCE > 0 ) % test that we found a close CE?
                                    if isempty(I) %|| ~( iCE > 0 ) % test that we found a close CE?
                                        % create new CE
                                        % restart iCE at end of centerTime
                                        iCE = size( centerTime, 2 ) + 1;
                                        % store the founding/base event
                                        centerTime(iCE) = baseEventTime; % founding event
                                        eventIncrement(iCE) = 1;
                                        CEstore( iCE, eventIncrement(iCE), :  ) = [ baseEventTime, iROI, ie ];

                                        % separate arrays for sorting
                                        times( iCE, eventIncrement(iCE) ) = baseEventTime;% this needs to be fancier, centered on mean of times 
                                        
                                        % store the test event
                                        eventIncrement(iCE) = eventIncrement(iCE) + 1;
                                        CEstore( iCE, eventIncrement(iCE), :  ) = [ testEventTime, iROItest, ieTest ];
                                        % arrays for sorting
                                        times( iCE, eventIncrement(iCE) ) = testEventTime;% this needs to be fancier, centered on mean of times 
                                    else
                                        iCE = I;
                                        % store the test event
                                        eventIncrement(iCE) = eventIncrement(iCE) + 1;
                                        CEstore( iCE, eventIncrement(iCE), :  ) = [ testEventTime, iROItest, ieTest ];
                                        % alt arrays
                                        times( iCE, eventIncrement(iCE) ) = testEventTime;% this needs to be fancier, centered on mean of times 

                                    end
                                end
                                iFlag(iROItest,ieTest)=1;
                                %ieTest = Inf; % this event cannot be a member of another CE
                                % 911 this is not a good way to do this,
                                % probably break
                            end % is the delta t small enough?
                        end % test if event is already captured by a CE
                    end % loop over test events
                end % loop over test ROIs
            end % loop over base events
        end % loop over base ROIs
        % we're done collecting events now finalize
        % kill the zeros in times
        % times( n coordinated events, n event index ), 
        z_indices =  times==0 ;
        times(z_indices) = NaN;
        % sort each row of times // scrambles founding/base event!
        %orig_times = times;
        %CEstore.rawTimes = times;
        [ timeSorted, sortedEventIndices ] = sort( times, 2 ); % sorts each row, events within CE are now in temporal order
        % each row of sortedEventIndices is a CE in the original order
        % find the first event for each row of sortedEventIndices
        %firstEvents = sortedEventIndices( sortedEventIndices==1 );
        [ timeSorted, sortedCEindices ] = sortrows( timeSorted ); % sort by the first column, CE times now are in temporal order

        % also sort centerTimes
        centerTimeSorted = sort( centerTime ); % founding events are now in temporal order
        % this above might not be correct, is it possible centerTimes could be a
        % different sort order than times sorted by first element? 
        % there is overlap!
    else
        CEstore = NaN;
        times = NaN;
        timeSorted=NaN;
        centerTime=NaN;
        centerTimeSorted = NaN;


    end % if event total >1
end