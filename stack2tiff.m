function name = stack2tiff( stack )
    name = append(inputname(1),'.tiff');
    t = Tiff(name,'w');
    x1 = size(stack,2);
    y1 = size(stack,1);
    nframes = size(stack,3);
    tagstruct.ImageLength = y1; % image height
    tagstruct.ImageWidth = x1; % image width
    tagstruct.Photometric = Tiff.Photometric.LinearRaw; % https://de.mathworks.com/help/matlab/ref/tiff.html
    tagstruct.SamplesPerPixel = 1;
    tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky; % groups rgb values into a single pixel instead of saving each channel separately for a tiff image
    tagstruct.Software = 'MATLAB';

    switch class(stack)
        case 'double'
            % FIJI cannot read this!
            tagstruct.BitsPerSample = 64;
            tagstruct.SampleFormat=Tiff.SampleFormat.IEEEFP;
        case 'uint8'
            % works with FIJI
            tagstruct.BitsPerSample = 8;
        otherwise
            warning(append('unrecognized class! ', class));
    end

    setTag(t,tagstruct);
    write(t,squeeze(stack(:,:,1)));
    for k = 2:nframes
        writeDirectory(t);
        setTag(t,tagstruct);
        write(t,squeeze(stack(:,:,k))); %%%appends the next layer to the same file t
    end
    % do this for as many as you need, or put it in a loop if you can
    close(t) 
end