function h = plotAnalytes( analytes )

%function analytes = analyzeROIs( ROIdataStruct, smooth, derPreSmooth, derPostSmooth, Fs, Pfactor)
% smooth = 11;
% derPreSmooth = 1;
% derPostSmooth = 11;
%Fs = 10; % Hz, sampling frequency %% seconds %/60seconds = minutes
% P factor factor of 10th percentile rolling SD for prominence threshold
nFiles = size( analytes.File, 2 );
% width vs height
figure 
hold on
for iFile = 1 : nFiles
    nROI = size(analytes.File(iFile).ROI,2);
    for iROI = 1 : nROI
        w = analytes.File(iFile).ROI(iROI).width;
        p = analytes.File(iFile).ROI(iROI).prominence;
        h = scatter(w,p,"filled");

    end
end
xlabel('width (s)' ); %,'FontSize',16)
ylabel('prominence (dF/F0)' ); %,'FontSize',16)
title('distribution of width and peak' ); %,'FontSize',16)
% SD vs activity
figure 
hold on
for iFile = 1 : nFiles
    nROI = size(analytes.File(iFile).ROI,2);
    for iROI = 1 : nROI
        x = size(analytes.File(iFile).ROI(iROI).prominence,1);
        y = analytes.File(iFile).ROI(iROI).SD;
        h = scatter(x,y,"filled");

    end
end
xlabel('number of events' ); %,'FontSize',16)
ylabel('overall SD' ); %,'FontSize',16)
title('how does SD vary with activity' ); %,'FontSize',16)
% SD vs activity
figure 
hold on
for iFile = 1 : nFiles
    nROI = size(analytes.File(iFile).ROI,2);
    for iROI = 1 : nROI
        x = size(analytes.File(iFile).ROI(iROI).prominence,1);
        y = analytes.File(iFile).ROI(iROI).P;
        h = scatter(x,y,"filled");

    end
end
xlabel('number of events' ); %,'FontSize',16)
ylabel('20th percentile rolling SD, * P *' ); %,'FontSize',16)
title('how does P vary with activity' ); %,'FontSize',16)
% P vs SD
figure 
hold on
for iFile = 1 : nFiles
    nROI = size(analytes.File(iFile).ROI,2);
    for iROI = 1 : nROI
        p = analytes.File(iFile).ROI(iROI).P;
        w = analytes.File(iFile).ROI(iROI).SD;
        h = scatter(w,p,"filled");

    end
end
ylabel('P (20th percentile rolling SD)' ); %,'FontSize',16)
xlabel('overall SD' ); %,'FontSize',16)
title('how does SD vary? does P correct?' ); %,'FontSize',16)
end