%function updateOldData( datecode )
    %% read var from file

    iExp = 1;
    datecode = '20211111c21'; % indexible list of datecodes here

    disp( append( 'updating old data and ROIs: ', datecode ) );
    basename = datecode;
    % pulls data from large "complete.mat" files
    [base, hROI, basetiming ] = returnHandlesBase( basename ); 
    
    ROIdb(iExp).name = datecode;
    ROIdb(iExp).timing = basetiming;

    %% edit ROIs
    %hROI2 = append(datecode,'hROI');
    figure('Name', datecode );
    ROIdb(iExp).hROI = editROI(base,hROI);

    %%
    disp(append('starting roi processing: ',datecode));
    figure('Name', datecode );
    hROI = ROIdb(iExp).hROI;
    baseStats = roi2stats3D2( base, hROI ); % uses "raw" (cropped, smoothed etc)
    baseStats.timing = squeeze( basetiming );
    disp('done with ROI stats')

    % process ROI time course data for dF/F0
    %  appends dF/F0 data to baseStats structure
    disp('starting roi dFoF0')
    baseStats = processROIs( baseStats );
    displayROIs( baseStats );
    disp('done with roi dFoF0')
    
    % EXPORT TO IGOR and MATLAB
    fname = append( basename, '_Igor');
    export2igor( baseStats, fname ) % custom function for export ROI data
    % save entire base stats structure
    fname = append( basename, '_baseStats');
    save(fname, 'baseStats')
    disp('export of Igor data complete')
    
    % saves entire workspace, big file
    filename = append( basename, 'complete.mat');
    save(filename,'-v7.3')
    disp('saved: ', filename, ' please move data files to proper locations!')

%end