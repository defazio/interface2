function export2igor( statsStruct,fname )
% custom function to store processed ROIs
% to be read by Igor (or MATLAB)

n = size( statsStruct, 2 );
ncells = statsStruct.nROIs;
npoints = size( statsStruct.ROI(1).dFoF0.ROI, 1);

pen = zeros( npoints, ncells );
dz = zeros( npoints, ncells );
roi = zeros( npoints, ncells );
pensub = zeros( npoints, ncells );
roidf = zeros( npoints, ncells );
pendf = zeros( npoints, ncells );
pensubdf = zeros( npoints, ncells );
for i=1:ncells
    roi( :, i ) = statsStruct.ROI(i).ROI.meanZ(:);
    dz( :, i ) = statsStruct.ROI(i).deadzone.meanZ(:);
    pen( :, i ) = statsStruct.ROI(i).penumbra.meanZ(:);
    pensub( :, i ) = roi(:,i)-pen(:,i);

    roidf( :, i ) = statsStruct.ROI(i).dFoF0.ROI(:);
    pensubdf( :, i ) = statsStruct.ROI(i).dFoF0.pensub(:);
    %pendf( :, i ) = statsStruct(i).dFoF0.pen(:);
end
FFAdFoF0 = statsStruct.FFA.dFoF0;
timing = statsStruct.timing;
save(fname, 'timing', 'roidf', 'pensubdf','FFAdFoF0');
end