
%read tiff
    [file,path] = uigetfile('*.tif');
    if isequal(file,0)
       disp('User selected Cancel');
       return;
    else
       disp(['User selected ', fullfile(path,file)]);
    end
    mytiff = fullfile( path, file );
    t=imread(mytiff);
    imshow(t);
    clim('auto');
figure
    histogram(t);
%optimize image

% select ROIs and penumbra

% quantify relevant ROIs