function [ROIfreq, totalfreq, ROInevents] = eventFreq( analytes, tstart, tend )
% return the event freqency for the range start to end (time units)
    dur = tend - tstart;
    
    % loop over the recordings organized by file
    nFiles = size( analytes.File, 2 );
    totalfreq = NaN(nFiles,1);
    maxROIs = 25;
    ROIfreq = NaN(nFiles,maxROIs);
    ROInevents = NaN(nFiles,maxROIs);
    for iFile = 1 : nFiles
    
        nROI = max(size(analytes.File(iFile).ROI));
        ROIecount = 0;
        for iROI = 1 : nROI
            t = analytes.File(iFile).ROI(iROI).times;
            rt = t( t>=tstart ); % get events after tstart
            rt = rt( rt<=tend ); % remove events after tend
            
            nevents = numel(rt);
            ROIfreq( iFile, iROI ) = nevents/dur;
            ROInevents( iFile, iROI ) = nevents;
            ROIecount = ROIecount + nevents;
        end
        totalfreq(iFile) = ROIecount/dur;
    end
end