function [eventTimes, intervalsIndex, timesIndex] = coordination2( iFile, analytes, maxDelta )
    indexArray=[1,1];
    eventTimes = NaN;
    intervalsIndex=NaN;
    timesIndex=NaN;
    nROI = max(size(analytes.File(iFile).ROI));
   % eventTimes = NaN(nROI,maxEvents);
    maxEvents = 0;
    for iROI = 1 : nROI
        times = analytes.File(iFile).ROI(iROI).times;
        nEvents = size( times, 1 );
        if nEvents > maxEvents
            maxEvents = nEvents;
        end
        if nEvents > 0
            for iEvent = 1:nEvents
                eventTimes( iROI, iEvent ) =  times( iEvent, 1 );
            end
        else
            eventTimes(iROI,1) = NaN;
        end
    end
    if maxEvents > 1
        indices =  eventTimes==0 ;
        eventTimes( indices ) = NaN;
        intervals = NaN( nROI, maxEvents, nROI, maxEvents );
        times = NaN( nROI, maxEvents, nROI, maxEvents );
        flag = zeros( nROI, maxEvents);
        index = 1;
        for iROI = 1 : nROI
            for iEvent = 1:maxEvents
                minInt = inf; % finding the shortest interval for this event
                for xROI = iROI : nROI 
                    firstEvent = 1;
                    if xROI == iROI % never look backwards
                        if firstEvent < maxEvents
                            firstEvent = iEvent + 1;
                        end
                    end
                    for xEvent = firstEvent:maxEvents
                        if ~flag(iROI,iEvent)
                            it = eventTimes( iROI, iEvent );
                            if xROI > nROI
                                display(num2str([iFile,iROI,iEvent,it,xROI,xEvent,xt]));
                            end
    
                            xt = eventTimes( xROI, xEvent );
                            if ~isnan(it) && ~isnan(xt) %( abs(it-xt)< maxDelta )
                                thisInt = it-xt;
    %                             if thisInt == 0
    %                                 display(num2str([iROI,iEvent,it,xROI,xEvent,xt]));
    %                             end
                                if abs(thisInt) < minInt
                                    minInt = abs(thisInt);                            
                                    %index = index + 1;
                                    intervalsIndex( index ) = thisInt;
                                    timesIndex( index )= it;
                                    indexArray = [ xROI, xEvent ];
                                end
                            end
                        end
                       
                    end % loop over x events
                end % loop over x ROIs
                flag( indexArray ) = 1;
                index = index+1;
            end % loop over iEvents
        end % loop over iROIs
        indices = intervalsIndex == 0;
        timesIndex( indices ) = NaN;
        intervalsIndex( indices ) = NaN;
    end % if there are more than 1 events
end
