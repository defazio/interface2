function [ smth, SD, der, SDder ] = prepdispROIstack( iROI, ROIstack, dx, smooth, derPreSmooth, derPostSmooth)
    if iROI > size(ROIstack,2)
        disp(append('exceeded number of ROIs: ',num2str(iROI)));
        iROI = 1;
    end
    trace = ROIstack(:,iROI); %ROIstats(iROI).dFoF0.ROI; %pensub;
    smth = boxcar( trace , smooth );
    SD = std(smth);
    %dx = 0.05; % sec
    prewin = derPreSmooth; % boxcar smooth window
    postwin = derPostSmooth;
    der = diffSmooth( smth, dx, prewin, postwin );
    SDder = std(der);
end