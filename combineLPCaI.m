% order processing script 2023 may 8
% VVV run this script to get CorrLagStore VVV
% plotCorrLagScript
% run this script to load LP data
% readLPcsv.m
%
% get list of LP data
LPnames = lpdata.Properties.VariableNames;
% should be a list of pairs, scx, scy with date code

xstart = 0;
xend = 600;
iFile = 11;
%for iFile = 1:nFiles

    arr = ROIdataStruct(iFile).data.pensubdf; % ntimepoints x nROIs
    xdata = ROIdataStruct(iFile).xdata;
    
    titleStr = append(num2str(iFile),' ',ROIdataStruct(iFile).name);
    h = figure('Name',titleStr, 'NumberTitle','off');
    tiledlayout(4,1)
    
%ax1 ROIs
    fighandle.ax1 = nexttile; % ([2 1]);

    plot(xdata, arr)
    xlim([xstart xend])
    title(titleStr)
    hold on

    nROI = size( arr, 2 );
    % rrmaxmean is the mean of the best correlations
    rrmaxmean = CorrLagStore(iFile).Cmaxmean; % = rrmaxmean;
    t = CorrLagStore(iFile).t;

    % parameters for detecting CEs using moving correlation data
    Croll_win = 1; % seconds
    Cprcthresh = 50; % percentile threshold for noise vs CE
    CPfactor = 10;
    CE_win = 10; % seconds, peaks within win are ignored

    [CElocs,CEpks] = findCEs( rrmaxmean, Croll_win, Cprcthresh, CPfactor, CE_win, Fs );

    CorrLagStore(iFile).CElocs = CElocs;
    CorrLagStore(iFile).CEpks = CEpks;

    CE_win = 5;
    % find events within CE window
    nCEs = size(CElocs,1);
    CEe = NaN( nCEs, nROI ); % event times for each ROI for each CE
    deltaCEe = NaN( nCEs, nROI ); % event times for each ROI for each CE
    for iCE = 1:nCEs
        iCEe = 0; % event counter for this correlated event
        xs = CElocs(iCE) - CE_win;
        xe = CElocs(iCE) + CE_win;
        % adds xlines to plot to demarcate CEs
        xline(xs)
        xline(xe)
    end

    nROI = size( arr, 2 );
    for iROI = 1: nROI
        timesCrossings = analytes.File(iFile).ROI(iROI).crossings;
        crossings = analytes.File(iFile).ROI(iROI).crosslev;
        scatter( timesCrossings, crossings )

        timesPeaks = analytes.File(iFile).ROI(iROI).times;
        pks = analytes.File(iFile).ROI(iROI).peaks;

        nE = size(pks,1);
        % events after xs
        for iCE = 1:nCEs
            iCEe = 0; % event counter for this correlated event
            xs = CElocs(iCE) - CE_win;
            xe = CElocs(iCE) + CE_win;

            events = timesCrossings(  timesCrossings>xs & timesCrossings<xe );
            % there should not be more than one!
            nCEe = size(events,1);
            if( nCEe == 0 )
                %disp(append('no events!'))
            else
               % disp('event added')
                CEe( iCE, iROI ) = events(1); % the first event to meet criteria
                deltaCEe( iCE, iROI ) = events(1) - xs;
            end
        end
    end % loop over ROIs
   
    % get the order of ROI activation
    deltaCEe(deltaCEe==0)=NaN;
    [sortedDCEe, I] = sort( deltaCEe, 2 );
    I(isnan(sortedDCEe))=NaN;

    nCE2 = size(deltaCEe,1);
    CElocsTrim = CElocs(1:nCE2,:);
    
    order = NaN(nCE2,nROI);
    for iCE = 1:nCE2
        thisCE = I(iCE,:);
        for iROI = 1:nROI
            [row,col]=find(thisCE==iROI,1);
            %disp(append(num2str(iCE),' roi: ',num2str(iROI),' place ', num2str(col)))
            if size(col,2)>0 
                order(iCE,iROI)=col;
            end
        end
    end
    golf_score = sum(order,1,"omitnan");

    % plots and more plots!!
    % raster

% ax2 RASTER
    fighandle.ax2 = nexttile;
    nofigure = 1;
    hTDR = TDrasterIndex(analytes,iFile,nofigure);
    xlim([xstart xend])
    
    % movcorr  

% ax3 CORRELATION
    fighandle.ax3 = nexttile; % ([2 1]);
    C = CorrLagStore(iFile).C; % = C;
    L = CorrLagStore(iFile).L; % = L;
    t = CorrLagStore(iFile).t; % = t;
    rrmaxmean = CorrLagStore(iFile).Cmaxmean; % = rrmaxmean;
    rrBestLagSorted = CorrLagStore(iFile).Cbestlag; % = rrBestLagSorted;
    p = plot(t, rrmaxmean );
    xlim([xstart xend])
    str=append('correlation ', num2str(k/Fs), 'sec window');
    ylabel(str)
    % add CE pks
    hold on
    scatter(CElocs,CEpks)

    % % plot lag
    % fighandle.ax4 = nexttile;
    % 
    % % note the array is rotated for display!!!!
    % y = 1:size(C,3); % y is index of pairs
    % hi = image(t,y,rrBestLagSorted', 'CDataMapping','scaled'); % t is time, y pair index
    % cb = colorbar;
    % ylabel( cb, 'lag (seconds)', 'Rotation', 270 )
    % ylim( cb, [ -maxlag_time, maxlag_time ] )
    % xlim([xstart xend])
    % ylabel 'sorted pairs'
    % xlabel 'time (seconds)'

%ax 5 loose patch
    fighandle.ax5 = nexttile; %([2 1]);
    % hold on
    % for iROI = 1:nROI
    %     y = order(:,iROI); %squeeze(deltaCEe(:,iROI));
    %     t = CElocsTrim; % 1:size(t,1);
    %     plot(t,y)
    % end    
    hold on
    iROI = 1; % first ROI should be loose patch
    %plot(xdata, arr(:,iROI))
    %ylabel 'targeted ROI'
    timesCrossings = analytes.File(iFile).ROI(iROI).crossings;
    crossings = analytes.File(iFile).ROI(iROI).crosslev;
    %scatter( timesCrossings, crossings )
    for iCE = 1:nCEs
        iCEe = 0; % event counter for this correlated event
        xs = CElocs(iCE) - CE_win;
        xe = CElocs(iCE) + CE_win;
        % adds xlines to plot to demarcate CEs
        xline(xs)
        xline(xe)
    end
    yyaxis right
    plot(scx20220209a,scy20220209a)
    xlim([xstart xend])
    ylabel 'targeted ROI APs'

    linkaxes([fighandle.ax1,fighandle.ax2,fighandle.ax3,fighandle.ax4,fighandle.ax5], 'x' )

    CorrLagStore(iFile).CEe = CEe;
    CorrLagStore(iFile).deltaCE = deltaCEe;
    CorrLagStore(iFile).order = order;
    CorrLagStore(iFile).golf_score = golf_score;
%end % loop over files
