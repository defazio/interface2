function ret = seeker( path, fname, data )
% pulls matlab variables out of saved data files
% code for "complete" workspaces
%completeDir = dir(path);
fullpathname = append(path, '/', fname);
%nComp = size(completeDir,1);
%S = load( fullpathname, base );
m = matfile( fullpathname );
varlist = who(m,data);

if ~isempty(varlist) % isfield(m,data)
    ret = m.(data);
else
    disp(append('variable: ', data, " doesn't exist, try again?"));
    ret = NaN;
end

end