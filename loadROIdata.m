% import data from *_Igor.mat files (contains ROI data)
function ROIdataStruct = loadROIdata( dx, localpath )
disp('WARNING! VERIFY XDATA!')
folderInfo = dir;
currentFolder = folderInfo(1).folder;

% get the list of file names
upath = userpath; % default user directory "MATLAB" contains data directory "ROI data"
dpath = append(upath,localpath); % code for ROI stacks output to Igor
ROIdirStruct = dir(dpath);
nROIstacks = size(ROIdirStruct,1);
for i=1:nROIstacks
     ROIdataName = ROIdirStruct(i).name;
     ROIdataStruct(i).name = ROIdataName(1:end-9); % removes '_Igor.mat' from file name
    fullpathname = append(ROIdirStruct(i).folder, '/', ROIdirStruct(i).name);
     data = load( fullpathname, 'pensubdf' ); %ROIdataName, 'pensubdf');
     ROIdataStruct(i).data = data;
     ROIstack = cell2mat(struct2cell(data));
     n = size(ROIstack,1);
     ROIdataStruct(i).xdata = dx * (0:(n-1));
     %assignin( "base", ROIvarName, data );
end
d = dir(currentFolder);
end