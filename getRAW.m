% import data from *_Igor.mat files (contains ROI data)
function outStruct = getRAW( localpath )
% use wildcard in local path to include/exclude files
% e.g. *Igor or *complete

folderInfo = dir;
currentFolder = folderInfo(1).folder;

% get the list of file names
upath = userpath; % default user directory "MATLAB" contains data directory "ROI data"
dpath = append(upath,localpath); % code for ROI stacks output to Igor
dirStruct = dir(dpath);
nStacks = size(dirStruct,1);
for i=1:nStacks
     dataName = dirStruct(i).name;
     outStruct(i).name = dataName; %(1:end-9); % removes '_Igor.mat' from file name
     fullpathname = append(dirStruct(i).folder, '/', dirStruct(i).name);
     S = load( fullpathname ); %, 'pensubdf' ); %ROIdataName, 'pensubdf');
     if isfield(S.baseStats,'ROI')
         nROI = size(S.baseStats.ROI,2);
         npnts = size(S.baseStats.ROI(1).ROI.meanZ, 2);
         ROIs = NaN(npnts,nROI);
         pens = NaN(npnts,nROI);
         ROIs_minBF = NaN(npnts,nROI);
         pens_minBF = NaN(npnts,nROI);
         for j=1:nROI
             ROIs(:,j)=S.baseStats.ROI(j).ROI.meanZ;
             pens(:,j)=S.baseStats.ROI(j).penumbra.meanZ;
             ROIs_minBF(:,j)=S.baseStats.ROI(j).dFoF0.minBF;
             pens_minBF(:,j)=S.baseStats.ROI(j).dFoF0.minBF;
         end
         outStruct(i).RAW = ROIs;
         outStruct(i).pen = pens;
         outStruct(i).RAW = ROIs_minBF;
         outStruct(i).pen = pens_minBF;
     else
        disp(append('*** no baseStats ***', dataName))
     end
     % ROIstack = cell2mat(struct2cell(data));
     % n = size(ROIstack,1);
     % nel = max(size(outStruct(i).data)); % get the number of time points
     % if isfield(S,'timing')
     %     %outStruct(i).xdata = S.timing(1:nel);
     %     dx = mean(diff(S.timing));
     %     if dx<0 
     %         dx=0.1;
     %     end
     %     disp('gathered FFA...',outStruct(i).name);
     %     outStruct(i).xdata = 0:dx:((nel-1)*dx);
     %     %disp(append('*** ', dataName,': no xdata, faked interval of 0.05s ',num2str(nel),' psdf: ',num2str(size(S.pensubdf))));
     % end
end
d = dir(currentFolder);
end