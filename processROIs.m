function ROIstats = processROIs(ROIstats)
    t1 = 50;
    t2 = inf;
    t4 = 2000; % need to upgrade to time, not steps
    
    %n = size( ROIstats, 2 ); % number of ROIs
    nROIs = ROIstats.nROIs; % ROIstats(n).nROIs;

    t = size( ROIstats.ROI(1).ROI.meanZ, 2 );
    stack = zeros( 2, nROIs+1, t );
    
    % load stack array with raw and trace data (trace is pen subtracted)
    % both will be processed by the deltaFoverFzero function
    % here stack is like a movie where each "pixel" is an ROI time course
    for i=1:nROIs % loop over ROIs
        raw = ROIstats.ROI(i).ROI.meanZ;
        pen = ROIstats.ROI(i).penumbra.meanZ;
        trace = raw - pen;
        stack( 1, i, : ) = raw; % position 1 of stack is type RAW, position 2 is roi
        stack( 2, i, : ) = trace; % position 1 of stack is type RAW-sub pen, position 2 is roi
    end
    stack(1,nROIs+1,:) = ROIstats.FFA.raw;
    stack(2,nROIs+1,:) = 1;

    [ dFoF0, minBF ] = deltaFoverFzero( stack, t1, t2, t4 );
    
    % load the dFoF0 data into the ROIstats structure and return
    
    for i=1:nROIs % loop over ROIs to store the processed data
        ROIstats.ROI(i).dFoF0.ROI = squeeze( dFoF0( 1, i, : ) );
        ROIstats.ROI(i).dFoF0.pensub = squeeze( dFoF0( 2, i, : ) );
    end
    ROIstats.FFA.dFoF0 = squeeze( dFoF0(1,nROIs+1,:) );

end