function ROIstats = roi2statsImage( im, hin )
% h contains handles for ROI polygons
% imstack is the raw data stack
    tic
    val = NaN;
    nROIs = size( hin, 2 ); % number of ROIs
    %dz = size( im, 3 ); % number of frames
    %maxProj = projectMax(im);
    him = imshow(im);
    clim( 'auto' );
    axis image
    axis on
    hold on
    msize = 1; 
    lsize = 1;
    for j = 1 : nROIs % loop over each ROI, loop over frames is within

        penpoly = hin(j).penumbra;
        deadpoly = hin(j).deadzone;
        roipoly = hin(j).ROI;
        
        h(j).handles.penumbra = drawpolygon(gca,'Position', penpoly, 'LineWidth', lsize, 'MarkerSize', msize,  'Color', [1,0,1], FaceAlpha=0 );
        h(j).handles.deadzone = drawpolygon(gca,'Position', deadpoly, 'LineWidth', lsize, 'MarkerSize', msize, 'Color', [1,1,1], FaceAlpha=0 );
        h(j).handles.ROI = drawpolygon(gca,'Position', roipoly, 'FaceAlpha', 0, 'LineWidth', lsize, 'MarkerSize', msize );

        % binary image %% code structure stolen from image analyst (again)
        roiImage = h(j).handles.ROI.createMask(him);
        deadzoneImage = h(j).handles.deadzone.createMask(him);
        penumbraImage = h(j).handles.penumbra.createMask(him);
        
        % Calculate the area, in pixels, that they drew.
        numPixROI = sum( roiImage(:) );
        numPixDead = sum( deadzoneImage(:) );
        numPixPen = sum( penumbraImage(:) );
        
        % Another way to calculate it that takes fractional pixels into account.
        numberOfPixels2 = bwarea( roiImage );
        % Mask the image and display it.
        % Will keep only the part of the image that's inside the mask, zero outside mask.
        % Calculate the mean
        %meanZ = zeros(dz,1);
        %slice = zeros(dx,dy);
        % dz=1;
        % for i=1:dz % loop over frames, this is true brute force
        i=1;
            sliceROI = im;
            sliceDead = im;
            slicePen = im;
    
            sliceROI( ~roiImage ) = val; % wipe out everything but the ROI
            sliceDead( ~deadzoneImage ) = val; % wipe out everything outside deadzone
            sliceDead( roiImage ) = val; % wipe out inside ROI to make donut
            slicePen( ~penumbraImage ) = val; % wipe out outside penumbra
            slicePen( deadzoneImage ) = val;  % wipe out inside deadzone to make 
                                            % penumbra donut
            
            meanROI = mean( sliceROI( roiImage ) );
            meanDeadzone = mean( sliceDead( deadzoneImage ), 'omitnan' );
            meanPenumbra = mean( slicePen( penumbraImage ), 'omitnan' );
     
            %save the masks!
            ROI(j).ROI.mask = roiImage; % = h(j).handles.ROI.createMask();
            ROI(j).deadzone.mask = deadzoneImage; % = h(j).handles.deadzone.createMask();
            ROI(j).penumbra.mask = penumbraImage; % = h(j).handles.penumbra.createMask();
            ROI(j).ROI.meanZ( i ) = meanROI ;
            ROI(j).deadzone.meanZ( i ) = meanDeadzone ;
            ROI(j).penumbra.meanZ( i ) = meanPenumbra ;
            values(j) = meanROI - meanPenumbra;
            pens(j) = meanPenambra;
        % end % ends loop over slices (frames)
        
        ROI(j).ROI.area1 = numPixROI;
        ROI(j).deadzone.area1 = numPixDead;
        ROI(j).penumbra.area1 = numPixPen;
        ROI(j).ROI.area2 = numberOfPixels2;
    end % ends loop over ROIs
    
    % add an additional entry to ROIstats
    % full frame average
    FFA = mean(im); % mean over x-y
    ROIstats.ROI = ROI;
    ROIstats.FFA.raw = squeeze(FFA);
    ROIstats.nROIs = nROIs;
    ROIstats.values = values;
    ROIstats.pens = pens;

    disp(append(' roi2stats3d: ', num2str(toc) ) );
end