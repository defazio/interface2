function h = TDraster( analytes, each )
% set each to something to make separate plots, its a string
spacing = 10; % units are the number of ROIs
nFiles = size( analytes.File, 2 );
% width vs height
grey=0.75;
fontSize = 24;
if strlength(each)==0
    h = figure('Color',[grey,grey,grey]); 
    hold on
    fontsize(gca,fontSize,'points')
    set(gca,'color',[grey,grey,grey])
end
%nROI=0;% used to increment y plot
y = 0; % nFiles * 20;
for iFile = 1 : nFiles
    iFile
    y = y + spacing; %nROI;
    if strlength(each)>0
        h = figure('Color',[grey,grey,grey],'Name',num2str(iFile)); 
        hold on
        fontsize(gca,fontSize,'points')
        set(gca,'color',[grey,grey,grey])

        y=0;
    end
    nROI = max(size(analytes.File(iFile).ROI));
    maxEvents = 1000;
     pAll = NaN( nROI, maxEvents );
     for iROI = 1 : nROI
         p = analytes.File(iFile).ROI(iROI).prominence;
         nevents = numel( p );
         for iEvent = 1 : nevents
            pAll(iROI, iEvent)=p(iEvent);
         end
     end
     pAll = pAll(~isnan(pAll)); % KILL ALL NAN ELEMENTS!
     pmax = max( pAll, [], 'all' );

     % setup a color table for prominences
     ncolors = 256; %numel( pAll );
    % scale values to color indices
    colorFactor = ncolors / pmax;
    for iROI = 1 : nROI
        t = analytes.File(iFile).ROI(iROI).times;
        nevents = size(t,1);
        p = colorFactor * analytes.File(iFile).ROI(iROI).prominence;
        y = y + 1; %iROI;
        if nevents > 0 
            yplot = zeros(nevents,1);
            yplot = yplot + y;
            s = scatter( t, yplot, [], p, "filled" );
            axis off
        end
    end
    if strlength(each)>0
        %axis tight
        xlabel('time (s)','FontSize',24);
        ylabel('ROIs','FontSize',24);
        title('raster plot of detected events','FontSize',24);
    end
end % loop over files
if strlength(each)==0
    %axis tight
    xlabel('time (s)','FontSize',24);
    ylabel('ROIs','FontSize',24);
    title('raster plot of detected events','FontSize',24);
end

end