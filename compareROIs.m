%% compare ROIs
% folders
folder_baseline = "C:\Users\MoenterLab\Documents\MATLAB\tdDFoF0\sb222\baseline";
folder_treat = "C:\Users\MoenterLab\Documents\MATLAB\tdDFoF0\sb222";
% date codes
dc = "20240111a";
f1 = "01";
f2 = "02";
suffix = "complete";
fn_baseline = append( dc, f1, suffix, ".mat" ); % "20240110a01complete.mat";
fn_treat = append( dc, f2, suffix, ".mat" ); % "20240110a03complete.mat";
% get max projections for pair
fp_baseline = append( folder_baseline, "\", fn_baseline ); % full path
fp_treat = append( folder_treat, "\", fn_treat ); % full path
% get ROIs for pair
myVars = { "hROI", "maxp" }; %scientific word for owl poop
S_baseline = load( fp_baseline, myVars{:} );
S_treat = load( fp_treat, myVars{:} );
% show ROIs
ifig_baseline = figure;
im_baseline = imshow(S_baseline.maxp, InitialMagnification='fit');
clim('auto')

% test first ROI
r=10;
offset = 4;
[FOO, nROIs] = size(S_baseline.hROI);

for i=1:nROIs
    ROI1_center =  mean( S_baseline.hROI(i).ROI ) ; % get the center of the round ROI
    % revise the ROI
    hROI1rev = ROIfromClick3(ROI1_center(1),ROI1_center(2),S_baseline.maxp, ifig_baseline, r, offset );
    handles(i) = hROI1rev; 
end
% clim('auto')
% ifig_treat = figure;
% im_treat = imshow(S_treat.maxp, InitialMagnification='fit');
% clim('auto')