
% retrieve all key names
allKeys = arrayfun(@char, metadata.keySet.toArray, 'UniformOutput', false);
% retrieve all key values
allValues = cellfun(@(x) metadata.get(x), allKeys, 'UniformOutput', false);