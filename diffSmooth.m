function output = diffSmooth( input, dx, presmth, postsmth )
input = boxcar( input, presmth );
n=size(input,1);
input(n+1)=0;
dy = diff(input)/dx;
output = boxcar( dy, postsmth );
end