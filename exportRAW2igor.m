function exportRAW2igor( s,fname )
% custom function to store processed ROIs
% to be read by Igor (or MATLAB)

nSlices = size( s.FFA, 2 );

[ npoints, ncells ] = size(s.FFA(i).RAW);

pen = zeros( npoints, ncells );
dz = zeros( npoints, ncells );
roi = zeros( npoints, ncells );
pensub = zeros( npoints, ncells );
roidf = zeros( npoints, ncells );
pendf = zeros( npoints, ncells );
pensubdf = zeros( npoints, ncells );
for i=1:ncells
    roi( :, i ) = s.ROI(i).ROI.meanZ(:);
    dz( :, i ) = s.ROI(i).deadzone.meanZ(:);
    pen( :, i ) = s.ROI(i).penumbra.meanZ(:);
    pensub( :, i ) = roi(:,i)-pen(:,i);

    roidf( :, i ) = s.ROI(i).dFoF0.ROI(:);
    pensubdf( :, i ) = s.ROI(i).dFoF0.pensub(:);
    %pendf( :, i ) = statsStruct(i).dFoF0.pen(:);
end
FFAdFoF0 = s.FFA.dFoF0;
timing = s.timing;
save(fname, 'timing', 'roi');
end