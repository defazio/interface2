group = "sham orx";
% master
% open one dataset, preprocess, select ROIs

% run openETS, extracts image stack into memory
% get two files, baseline and treatment
disp("reading ETS and VSI...")
% rbase and rtreat raw data files, nbase ntreat meaningful names
[ baseRaw, timing, baseInterval, baseName, baseETS, metadata ] = openETSui(); % puts image stack into 'stack'
disp("ETS read into memory...")
%% motion correction
% run the td_moco macro
base = td_moco(baseRaw);
%% % get ROIs from user
%[ meanDFoF0, maxDFoF0, stdDFoF0 ] = projMeanMaxSTD(baseDFoF0);
base = single(base);
%stackin = single(stackin); %double( stackin );
meanp = mean( base, 3 );
%meanph = histeq(meanp);
maxp = max( base, [], 3 );
compStack(base,baseRaw,50)

%maxph = histeq(maxp);
%stdp = std( base, 0, 3 );
%dispMeanMaxSTD(meanStack, maxStack, stdStack);
base=uint16(base);
% SELECT ROIs
% NOTE: PROCESSES ONE EXPERIMENT AT A TIME
% master for ROI analysis
% assumes dFoF0 stack/movie is already generated 
%    (see master.m or load matlab.mat dataset)
% outline: 
%    get roi set (includes deadzone, penumbra), 
%    reprocess dF/F0 from "stack" (raw but includes crop, downsampling, etc)
%    display results
%    output to Igor? or process events here?
%%
% get ROIs from user
disp('PLEASE DO NOT CLOSE FIGURE! max 20 ROIs')
radius = 4;% pixels to search for half amplitude, use 2 pixel if spatial resize
offset = 2;% pixel separation between roi, deadzone, penumbra
% click2ROI2 is fixed size ROIs, independent of x-y profile 
%hROI = click2ROI2( maxDFoF0, radius, offset ); % handles contains info on ROIs
hROI = click2ROI2( maxp, radius, offset ); % handles contains info on ROIs
nROIs = size( hROI, 2 ); % number of ROIs

%% loop over ROIs to create time course data with pen subtraction
%treatStats = roi2stats3D( treat, hROI ); % uses "raw" (cropped, smoothed etc)
disp('starting roi processing')
baseStats = roi2stats3D( base, hROI ); % uses "raw" (cropped, smoothed etc)
baseStats.timing = squeeze(timing);
disp('done with ROI stats')
% process ROI time course data for dF/F0
%  appends dF/F0 data to baseStats structure
disp('starting roi dFoF0')
baseStats = processROIs2( baseStats );
%displayROIs( baseStats );
disp('done with roi dFoF0')

% EXPORT TO IGOR and MATLAB
fname = append( baseName, '_Igor');
export2igor( baseStats, fname ) % custom function for export ROI data
% save entire base stats structure
fname = append( baseName, '_baseStats');
save(fname, 'baseStats')
disp('export complete')

% saves entire workspace, big file 
filename = append( baseName, 'complete.mat');
save(filename,'-v7.3')
disp('export complete complete')