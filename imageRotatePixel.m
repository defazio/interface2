function imgRot = imageRotatePixel( img, pixelx, pixely, degrees )
% img is the image to rotate
% pixelx, pixely is the pixel to rotate about
% degrees is the amount of rotation

% get the size of the original
% make a bigger version to hold the entire translated/rotated image (no crop)
% translate the image so px,py are at the center
% rotate the image degrees
% translate the image back

end 