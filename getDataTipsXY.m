function txt = getDataTipsXY(~, info, imMat, ifig, dfig, iROI, ROIs)
%disp("in getDataTipsXY")
y = info.Position(1);
    x = info.Position(2);
    txt = ['(' num2str(x) ', ' num2str(y) ')'];
    txt = [''];
    [ xpnts, ypnts ] = size( imMat );
xx = linspace( 1, xpnts, xpnts );
yy = linspace( 1, ypnts, ypnts );
p1 = zeros( ypnts, 1 );
p1(:,1) = imMat( x, : );
p2 = zeros( xpnts, 1 );
p2(:,1) = imMat( : , y );

figure(dfig)
yyaxis left
plot(yy, p1)
yyaxis right
plot(xx, p2)

r = 7;
xxx = testimprofile( imMat, x, y, r );

figure(ifig)

h0 = images.roi.Freehand(gca,'Position', xxx, FaceAlpha=0 );
o = 4;
deadzone = xxx + [0,-o; o,-o; o,0; o,o; 0,o; -o,o; -o, 0; -o,-o];
h1 = images.roi.Freehand(gca,'Position', deadzone, 'Color', [1,1,1], FaceAlpha=0 );
o = 4;
penumbra = deadzone + [0,-o; o,-o; o,0; o,o; 0,o; -o,o; -o, 0; -o,-o];
h2 = images.roi.Freehand(gca,'Position', penumbra, 'Color', [1,0,1], FaceAlpha=0 );

ROIs(iROI).ROIh = h0;
ROIs(iROI).deadh = h1;
ROIs(iROI).penh = h2;

iROI = iROI + 1;

%  index = 1;
%  RGBtriplet = [ 1 0 0 ];
%  h1 = images.roi.Point(gca,'Position',deadzone(index,:),'Color',RGBtriplet);
%  index = 2;
%  RGBtriplet = [ 0 1 0 ];
%  h2 = images.roi.Point(gca,'Position',deadzone(index,:),'Color',RGBtriplet);
%  index = 3;
%  RGBtriplet = [ 0 0 1 ];
%  h3 = images.roi.Point(gca,'Position',deadzone(index,:),'Color',RGBtriplet);
%  index = 4;
%  RGBtriplet = [ 1 1 0 ];
%  h4 = images.roi.Point(gca,'Position',deadzone(index,:),'Color',RGBtriplet);
%  index = 5;
%  RGBtriplet = [ 0 1 1 ];
%  h5 = images.roi.Point(gca,'Position',deadzone(index,:),'Color',RGBtriplet);
%  index = 6;
%  RGBtriplet = [ 1 0 1 ];
%  h6 = images.roi.Point(gca,'Position',deadzone(index,:),'Color',RGBtriplet);
%  index = 7;
%  RGBtriplet = [ 1 1 1 ];
%  h7 = images.roi.Point(gca,'Position',deadzone(index,:),'Color',RGBtriplet);
% index = 8;
% RGBtriplet = [ 0 0 0 ];
% h8 = images.roi.Point(gca,'Position',deadzone(index,:),'Color',RGBtriplet);

%h2 = images.roi.Freehand(gca,'Position', xxxx );

end