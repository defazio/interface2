
% superimpose plots
iFile = 9;
xs = 230;
xe = 280;
ns = 3; % additional smoothing
thresh = 0.3;
[theseData, threshTime, superh] = superimposePlots( iFile, ROIdataStruct, xs, xe, ns, thresh );
ylim([-1,2])
hold on
yyaxis right
% xcorr
iFile = 9;
%for iFile=1:nFiles
    arr = ROIdataStruct(iFile).data.pensubdf;
    xdata = ROIdataStruct(iFile).xdata;
    %rotArr = rot90(arr);
    [rr,pp] = wrapperMovCorr(arr, 50);
    meanrr = mean( rr, 2 );
    str = append('iFile: ',num2str(iFile),' ',ROIdataStruct(iFile).name);
   %h = figure('Name',num2str(iFile)); 
    plot(xdata, meanrr);
%end
ylim([-0.2,5])