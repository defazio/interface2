% reorganize data in basestats 
function outStruct = getRAW2( baseStats )

    if isfield(baseStats,'ROI')
         nROI = size(baseStats.ROI,2);
         npnts = size(baseStats.ROI(1).ROI.meanZ, 2);
         ROIs = NaN(npnts,nROI);
         pens = NaN(npnts,nROI);
         ROIs_minBF = NaN(npnts,nROI);
         pens_minBF = NaN(npnts,nROI);
         for j=1:nROI
             ROIs(:,j)=baseStats.ROI(j).ROI.meanZ;
             pen(:,j)=baseStats.ROI(j).penumbra.meanZ;
             ROIs_minBF(:,j)=baseStats.ROI(j).dFoF0.ROIminBF;
             pensub_minBF(:,j)=baseStats.ROI(j).dFoF0.minBF;
         end
         outStruct.RAW = ROIs;
         outStruct.pensub = ROIs - pen;
         outStruct.RAW_mBF = ROIs_minBF;
         outStruct.pensub_mBF = pensub_minBF;
     else
        disp(append('*** no baseStats ***', dataName))
     end

end