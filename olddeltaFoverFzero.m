% calculate dF over F0
function [dFoverF0,minBF] = olddeltaFoverFzero( stack, t1, t2 ) %, minBarF )

% bar F "smoothing"
disp('starting BarFofT');
BarF = BarFofT( stack, t1 );

disp('starting minBarF. Prepare to wait a long while...');
minBF = minBarF_old( BarF, t2 );

% F0 is minBF
% df is raw - minBF
%height = size( stack, 1);
%width = size( stack, 2);
%frames = size( stack, 3);

%dFoverF0 = zeros( height, width, frames );
dblstack = double( stack );

disp('starting dF/F0');

tic
dFoverF0 = dblstack - minBF;
disp(append(' F-F0: ', num2str(toc) ) );
tic
dFoverF0 = dFoverF0 ./ minBF; % ./ means by element, otherwise need a scalar
disp(append(' dF / F0: ', num2str(toc) ) );

plot2axes( stack, BarF, minBF, dFoverF0, 100, 100 );
%tic
%enhanced = histeq( dFoverF0);
%toc
%implay(dFoverF0);
end                 