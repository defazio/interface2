%% structure ROIs derived from click or previous ROIs
function h = structureROIs( hROI, fax, im, r, offset )
% hROI is a set of ROI handles
% test first ROI
%r=10;
%offset = 4;
nROIs = size(hROI,2);

for i=1:nROIs
    M =  mean( hROI(i).ROI ) ; % get the center of the round ROI
    % revise the ROI
    h(i) = structuredROIax( M(1), M(2), im, fax, r, offset );
end

end