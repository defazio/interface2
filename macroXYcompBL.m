% macro compXY

for i=1:nFiles
    figure
    y = analytesBL.File(i).ROI.peaks;
    x = analytesBL.File(i).ROI.width;
    scatter(x,y)
    str = 'dF/F0';
    ylabel(str)
    str  = 'wdith (seconds)';
    xlabel(str);
    hold on
    y = analytes.File(i).ROI.peaks;
    x = analytes.File(i).ROI.width;
    scatter(x,y)
end
