function dispMeanMaxSTD( meanp,maxp,stdp )
% max projection, sd projection?
tic
subplot(1,3,1)
imshow(meanp)
clim('auto')
subplot(1,3,2)
imshow(maxp)
clim('auto')
subplot(1,3,3)
imshow(stdp)
clim('auto')
disp(append('project mean max std...',num2str(toc)));
end

