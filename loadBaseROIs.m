% function to load base and roi info from user selected complete.mat file
function S = loadBaseROIs( )
    [file,path] = uigetfile('*complete.mat');
    if isequal(file,0)
       disp('User selected Cancel');
       return;
    else
       disp(['User selected ', fullfile(path,file)]);
    end
    complete = fullfile( path, file );
    %exp = '\d{8}[a-z]_\d*';
    %name = regexp(complete,exp,'match','once');
    disp(append('master: opening file and extracting image stack', file));
    temp = load( complete ); %, "base", "hROI" ); %, "base", "");
    S.stack = temp.base;
    S.timing = temp.timing;
    S.interval = temp.baseInterval;
    S.name = temp.baseName;
    S.filename = temp.filename; % complete.mat
    S.ets = temp.baseETS;
    %S.metadata = temp.metadata;
    S.hROI = temp.hROI;
    S.group = temp.group;
    disp("load complete in app")
end