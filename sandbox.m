%%
baseName = '202310111b02';
group = 'orx';
%%
% need minBarF (rolling F0)
baseStats = processROIs2( baseStats );
%% set up roipens
clear ROIpens
ROIpens(1) = getRAW2( baseStats );
ROIpens(1).baseName = baseName;
ROIpens(1).group = group;

%% loop over slices
pdfname = append(baseName,'_ROIpens.pdf');
nslices = size(ROIpens,2);
slice = 1;
% loop over ROIs
%raw = ROIpens(slice).RAW(1:5001,)
nROIs = size(ROIpens(slice).RAW,2);

set(0,'DefaultFigureWindowStyle','docked')
set(0, 'DefaultAxesFontSize', 18);
set(groot,'defaultLineMarkerSize',10);

%dx = mean(diff(baseStats.timing)); % 0.1;

h=figure;
hold on
x1 = baseStats.timing/60000; % 0:dx:500;
y2 = baseStats.FFA.raw;
plot( x1,y2, 'r')
for ROI = 1:nROIs
    y1 = ROIpens(slice).RAW_mBF; %pensub_mBF; % (1:5001,ROI); % rows of RAW are time, columns are ROIs
    y3 = ROIpens(slice).RAW; % pensub; % (1:5001,ROI);

    % display the results

    plot( x1,y3,'k',  x1,y1,'b' )
    str = append( baseName, ' ROI: ', num2str(ROI), ' ', group );
    title(str)    
end
    legend('RAW','Konnerth F0')
    %legend('penumbra-subtracted','Konnerth F0')

    str = 'time (minutes)';
    xlabel(str)
    str=append(baseName,' fluorescence (arbitrary units)');
    ylabel(str)
