%%
baseName = '202310111a02';
group = 'orx';
%%
% need minBarF (rolling F0)
baseStats = processROIs2( baseStats );
%% set up roipens
clear ROIpens
ROIpens(1) = getRAW2( baseStats );
ROIpens(1).baseName = baseName;
ROIpens(1).group = group;
%% set up fits
% var set up 
slice = 1;
ROI = 1;
dx = 0.1;

fitxmin = 0;
fitxmax = 15; % seconds
ifitxmin = 1 + fitxmin/dx;
ifitxmax = 1 + fitxmax/dx;

x1=0:dx:500;
y1=ROIpens(slice).pensub(1:5001,ROI); % rows of RAW are time, columns are ROIs
x2=0:dx:fitxmax;
y2=ROIpens(slice).pensub_mBF(1:ifitxmax,ROI); % rows of RAW are time, columns are ROIs

% flat part
flat_xmin = 5; % seconds
flat_xmax = 30; % seconds
iflat_xmin = 1 + flat_xmin/dx;
iflat_xmax = 1 + flat_xmax/dx;
xflat = flat_xmin:dx:flat_xmax;
yflat = ROIpens(slice).pensub_mBF( iflat_xmin:iflat_xmax, ROI );
disp('realF0sandbox: var set up complete')
%%
% function for fitting double exponential 
% y = y0 + A * exp( - (x - x0) / t1 ) + B * exp( - (x - x0) / t2 )

%function coefs = cfit_dblexp( x, y )
% x are the x data points, usually time
% y are the y data points, some parameter like fluorescence
%fitstring = "y0 + A * exp( - (x - x0) / t1 ) + B * exp( - (x - x0) / t2 )";
fitstring = "y0 + A1 * exp( - x / tau1 ) + A2 * exp( - x / tau2 )";

ftype = fittype( fitstring, ...
    dependent="y", independent="x", ...
    coefficients=["y0" "A1" "tau1" "A2" "tau2"]);
%    coefficients=["y0" "A1" "x0" "tau1" "A2" "tau2"]);

f = fit( x2', y2, ftype, ...
       'StartPoint', [10000 1000 1 500 500], ...
       'lower', [0 0 0 0 0], ...
       'upper', [65000 65000 1000 65000 5000] );

plot(f,x1,y1)
xlim([-5 20])
disp('realF0sandbox: double exponential curve fit setup')
%% linear fit

% x are the x data points, usually time
% y are the y data points, some parameter like fluorescence
lfitstring = "Aslope * x + Bintercept";

lftype = fittype( lfitstring, ...
    dependent="y", independent="x", ...
    coefficients=["Aslope" "Bintercept"]);
%    coefficients=["y0" "A" "x0" "t1" "B" "t2"]);

lfit = fit( xflat', yflat, lftype,...
        'StartPoint', [-1 10000], ...
        'lower', [-inf -inf], ...
        'upper', [inf inf] ); %, ...
%    'lower', [-inf -inf], 'upper', [inf inf] );
figure
plot(lfit,'r')
hold on
plot(x1,y1,'b',xflat,yflat,'k')
xlim([-5 20])
disp('realF0sandbox: linear curve fit setup')
%% loop over slices
pdfname = append(baseName,'_ROIpens.pdf');
nslices = size(ROIpens,2);
slice = 1;
% loop over ROIs
%raw = ROIpens(slice).RAW(1:5001,)
nROIs = size(ROIpens(slice).RAW,2);

set(0,'DefaultFigureWindowStyle','docked')
set(0, 'DefaultAxesFontSize', 18);
set(groot,'defaultLineMarkerSize',10);

dx = 0.1;
fitxmin = 0;
fitxmax = 30; % seconds
ifitxmin = 1;
ifitxmax = 1 + fitxmax/dx;

% storage for fit results
% 7 fit parameters, max of 30 rois
fitresults = zeros( 7, nROIs );
ROIpens.fitVarNames = cat( 1, coeffnames(f), coeffnames(lfit) );

x1=0:dx:500;
for ROI = 1:nROIs
    y1=ROIpens(slice).pensub_mBF(1:5001,ROI); % rows of RAW are time, columns are ROIs
    x2=0:dx:fitxmax;
    y2=ROIpens(slice).pensub_mBF(1:ifitxmax,ROI); % rows of RAW are time, columns are ROIs
    y3 = ROIpens(slice).pensub(1:5001,ROI);
    yflat = ROIpens(slice).pensub_mBF( iflat_xmin:iflat_xmax, ROI );

    % double exponential fit
    dblexpf = fit( x2', y2, ftype, ...
        'StartPoint', [10000 1000 1 500 500], ...
        'lower', [0 0 0 0 0], ...
        'upper', [65000 65000 1000 65000 5000] );

%coefficients=["y0"  "A1"  "tau1"  "A2"  "tau2"]);
    fits{ROI} = dblexpf; % store the results for later

    % linear fit
    lfit = fit( xflat', yflat, lftype, ...
        'StartPoint', [-1 10000], ...
        'lower', [-inf -inf], ...
        'upper', [inf inf] );

    lfits{ROI} = lfit; 
    
    % store the results for later
    catCoeffvals = cat( 2, coeffvalues(dblexpf), coeffvalues(lfit) );
    fitresults(:,ROI)=catCoeffvals(:);

    % display the results
    h=figure;
    plot( x1,y3,'k.',  x1,y1,'b' )
    str = append( baseName, ' ROI: ', num2str(ROI), ' ', group );
    title(str)
    hold on
    plot(dblexpf)
    plot(lfit, 'k')
    xlim([-5 30])
    legend('penumbra-subtracted','Konnerth F0','double exp fit')
    str = 'time (seconds)';
    xlabel(str)
    str=append(baseName,' fluorescence (arbitrary units)');
    ylabel(str)
    
    coeffs = coeffnames(dblexpf);
    coeffvals = coeffvalues(dblexpf);
    ci = confint(dblexpf,0.95);
    str1 = sprintf('\n %s = %0.3f (%0.3f %0.3f)', coeffs{1}, coeffvals(1), ci(:,1) );
    str2 = sprintf('\n %s = %0.3f (%0.3f %0.3f)', coeffs{2}, coeffvals(2), ci(:,2) );
    str3 = sprintf('\n %s = %0.3f (%0.3f %0.3f)', coeffs{3}, coeffvals(3), ci(:,3) );
    str4 = sprintf('\n %s = %0.3f (%0.3f %0.3f)', coeffs{4}, coeffvals(4), ci(:,4) );
    str5 = sprintf('\n %s = %0.3f (%0.3f %0.3f)', coeffs{5}, coeffvals(5), ci(:,5) );
    dim=[0.4 0.5 0.3 0.3];
    t=annotation( 'textbox', dim, 'String',...
        [ fitstring, 'Coefficients (with 95% confidence bounds): ', ...
        str1, str2, str3, str4, str5],...
        'FitBoxToText', 'on' );
    t.FontSize = 18;
    exportgraphics(h,pdfname,'Append',true)
end
ROIpens.fitresults = fitresults;
fname = append( baseName, '_ROIpens');
save(fname, 'ROIpens')
disp('export ROIpens complete')