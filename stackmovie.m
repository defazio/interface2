function mov = stackmovie( stack )
cmap = jet;
stack8b = dbl2uint8( stack );
MFstack = stack2MFarray( stack8b );
mov = immovie( MFstack, cmap );
implay( mov, 100 );
end
