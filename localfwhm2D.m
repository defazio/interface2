function [ xw, yw ] = localfwhm2D( img, xx, yy, win )
% img is a 2d image array; note smoothing in preprocessing
% xx, yy is the location of a click for an ROI
% win is the 0.5 search width for local max and min

% from imageanalyst
% https://www.mathworks.com/matlabcentral/answers/310113-how-to-find-out-full-width-at-half-maximum-from-this-graph
% Find the half max value.
% y direction
%data = zeros(1, win);

% NEEDS ERROR CHECKING NEAR EDGES of image!!!

data = img( xx, yy-win:yy+win );
halfMax = ( min( data, [], 2 ) + max( data, [], 2 )) / 2;
% Find where the data first drops below half the max.
index1 = find( data >= halfMax, 1, 'first' );
% Find where the data last rises above half the max.
index2 = find( data >= halfMax, 1, 'last' );
yw = index2-index1 + 1; % FWHM in indexes.

% x direction
%data = zeros( win, 1 );
data = img( xx-win:xx+win, yy );
halfMax = ( min( data, [], 1 ) + max( data, [], 1 )) / 2;
% Find where the data first drops below half the max.
index1 = find( data >= halfMax, 1, 'first' );
% Find where the data last rises above half the max.
index2 = find( data >= halfMax, 1, 'last' );
xw = index2-index1 + 1; % FWHM in indexes.

%disp("leaving localfwhm")
end