% REPLACES ROIPROCCESSING.MLX!!!

%STEP 3: after selecting ROIs, now detect events (can process all files in localpath)
% NOTE: THIS PROCESSES ALL FILES IN 'localpath' FOLDER
% multiple experiments can be processed sequentially

% select file
% this should be by file, dx should be consistent, but xs,xe changes?
<<<<<<< HEAD
%localpath = '/tdDFoF0/sb222/baseline/*_Igor.mat';
localpath = '/tdDFoF0/sb222/*_Igor.mat';
=======
localpath = '/tdDFoF0/sb222/baseline/*_Igor.mat';
>>>>>>> parent of 9ec7d79 (working on revising ROI)
%localpath = '/roipens_igor/males/intact/*_Igor.mat';
%localpath = '/roipens_igor/males/orx/*_Igor.mat';%localpath = '/ROIdataLT/orx/*_Igor.mat';
%localpath = '/roipens_igor/males/sham orx/*_Igor.mat';
%localpath = '/ROIdataLT/orx/*_Igor.mat';
%localpath = '/ROIdataLT/sham intact/*_Igor.mat';
%localpath = '/ROI data/*_Igor.mat'; % original data set 10min
%localpath = '/ROI data 5 min/*_Igor.mat'; % original data set 10min
%localpath = '/EGTA/*_Igor.mat';
% check if older files missing variables

% reprocess older files

% proceed
ROIdataStruct = loadROIdata2(localpath);
set(0, 'DefaultAxesFontSize', 18);
set(groot,'defaultLineMarkerSize',10);
nFiles = size( ROIdataStruct, 2 );
set(0,'DefaultFigureWindowStyle','normal')
figure
set(0,'DefaultFigureWindowStyle','docked')
%%

% all values in seconds
smooth = 1; % 7 works, hideous offset associated with boxcar, use 1 to bypass
derPreSmooth = 1;
derPostSmooth = 1; % 10 for visibility
% find peaks

% need to handle timing individually??

%Fs = 1/dx; % Hz, sampling frequency %% seconds %/60seconds = minutes
roll = 10; % seconds

prcthresh = 20;
Pfactor = 10;
% settings for peak detection
%minHeight = Pfactor * P
minwidth = 1;% / dx; % time in seconds / dx to get width in points
%widthRef = 'halfprom'; %'halfheight'; %
%minPeakProm = minHeight; % Pfactor * P; % dF/F0 units

analytes = analyzeROIs2( ROIdataStruct, roll, minwidth, smooth, ...
    Pfactor, prcthresh );
disp('completed event analysis...')

% combine all events for each file
events = linearize( analytes );
%%

% old style display
binsize = 10/60; % now minutes % seconds

%bindata = NaN(nFiles,nbins);
ymin = -0.02; %min(ydata,[],"all");
ymax = 0.05; %max(ydata,[],"all");

%iFile = 9;
for iFile=1:nFiles

    xend = max(ROIdataStruct(iFile).xdata)/60;
    xstart = 0;
    dx = mean(diff(ROIdataStruct(iFile).xdata)); % seconds
    Fs = 1/dx; % Hz
    %nbins = round(1 + (xend - xstart) / binsize );

% stack
    %figure
    sh = stackplots2(iFile, ROIdataStruct, ymin, ymax );
    figure

% raster 1
    tiledlayout(3,1)
    nexttile
    nofigure =1;
    hTDR = TDrasterIndex(analytes,iFile,nofigure);
    %xlim([0 xend])

% time histo 2
    nexttile
    ts = events(iFile,:);
    xstart = 0; 
    xend = max(ts);
    %[bindata(iFile,:)] = timeSeriesHistogram( ts, binsize, xstart, xend );
    timeSeriesHistogram( ts, binsize, xstart, xend );

% movcorr -old version    
    nexttile
    arr = ROIdataStruct(iFile).data;%.pensubdf;
    xdata = ROIdataStruct(iFile).xdata;
    %rotArr = rot90(arr);
    k = 50;
    %[rr,pp] = wrapperMovCorr(arr, k);
    %function [ rr, ll, t ] = wrapperCorrgram( xx, k, Fs, maxlag, frac_overlap )
    %maxlag_time = 5; % seconds
    %maxlag = maxlag_time * Fs;
    %frac_overlap = 0.5; 
    
    [ rr, ll ] = wrapperMovCorr( arr, k ); %, Fs, maxlag, frac_overlap );
    % alternative correlation algorithms
    %    [ rr, ll, t ] = wrapperCorrgram( arr, k, Fs, maxlag, frac_overlap );
    %    [ rr, ll, t ] = wrapperMIgram( arr, k, Fs, maxlag, frac_overlap );
    %    [ rr, ll, t ] = wrapperTWX( arr, k, Fs, maxlag, frac_overlap );

     meanrr = mean( rr, 2 );
    str = append('iFile: ',num2str(iFile),' ',ROIdataStruct(iFile).name);
    %h = figure('Name',num2str(iFile)); 
    plot(xdata, meanrr);
    title(str);
    ylim([-0.1 1])
    xlim([0 xend])
end