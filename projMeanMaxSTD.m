function [meanp,maxp,stdp] = projMeanMaxSTD( stackin )
% max projection, sd projection?
tic
stackin = single(stackin); %double( stackin );
meanp = mean( stackin, 3 );
%meanph = histeq(meanp);
maxp = max( stackin, [], 3 );
%maxph = histeq(maxp);
stdp = std( stackin, 0, 3 );
%stdph = histeq(stdp);
%montage({ meanph, maxph, stdph}, 'Size', [1 3], 'DisplayRange', []);
subplot(1,3,1)
imshow(meanp)
clim('auto')
subplot(1,3,2)
imshow(maxp)
clim('auto')
subplot(1,3,3)
imshow(stdp)
clim('auto')
disp(append('project mean max std...',num2str(toc)));
end

