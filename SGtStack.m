% smooth over time S-G baby!
function smthStack = SGtStack( stack, order, framelen )
%order = 3;
%framelen = 11;
%sm(:,1) = s1(x,y,:);
tic
height = size( stack, 1);
width = size( stack, 2);
frames = size( stack, 3);
sz = [ height, width ];
pixels = height * width;
k = ones(9,1); %kaiser(framelen,38);

tmpStack = sgolayfilt(stack,order,framelen,k,3);
tmpStack = squeeze(tmpStack);
smthStack = zeros( height, width, frames );
for p=1:pixels
    [h,w]=ind2sub(sz,p);
    smthStack(h,w,:)=tmpStack(p,:);
end

str = append('savitsky-golay filer time: ', num2str(toc), ' seconds' );
disp(str);
end