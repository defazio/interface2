function closest = findclosest( A, B, minDiff,sign )
% for each element A find the closest element of B
% if A-B < minDiff, return diff, else return zero
nA = size( A, 1 ); % assumes first dim is relevant
nB = size( B, 1 );
if ( nA>1 ) && ( nB>1 )
    for i=1:nA
        [diff,j] = min( A(i) - B );
else
    closest = 0;
end
    
