% plot corr with lag
function plotRasterBinsMovcorr( xdata, arr, C, L, T, Fs, title_name )
    figure('name',title_name, ...
        'Units','inches', ...
        'Position', [1, 1, 7, 10.75] );
    ymin = -0.02;
    ymax = 0.05;
    xmin = 0;
    xmax = 600;

    tiledlayout(3,1)
    
    nexttile
    %stackplotsArray( xdata, arr, ymin, ymax );
    %xlim([xmin xmax])

    nexttile    
    % C is lag X time X comp
    t = T; % center times of the window
    y = 1:size(C,3);
    lag_time = L; % lag times
    
    [ rrmax, rrmaxi ] = max( C, [], 1 ); % get the max correlation over lag
    % and keep the index for lag
    
    % take the mean over all the comparisons
    rrmaxmean = mean( rrmax, 3 );
    plot(t, rrmaxmean )
    title( title_name )
    xlim([xmin xmax])
    ylabel 'correlation'
    
    if size(L,1)>1
        nexttile
        rrmaxi = squeeze(rrmaxi); % eliminates dimension of 1
        rrbestlag=(rrmaxi-100)/Fs; % convert index to lag time
        meansOfCols = mean( rrbestlag, 1 );
        [ sortedMeans, sortOrder ] = sort( meansOfCols );
        rrbestlagSorted = rrbestlag( :, sortOrder );
        % note the array is rotated for display!!!!
        image(t,y,rrbestlagSorted', 'CDataMapping','scaled')
        cb = colorbar;
        ylabel( cb, 'lag (seconds)', 'Rotation', 270 )
        xlim([xmin xmax])
        ylabel 'sorted pairs'
        xlabel 'time (seconds)'
    end
end
