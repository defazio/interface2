function [ydataRange, threshTime, sh ] = superimposePlots( iFile, ROIdataStruct, xs, xe, nsmooth, thresh )
% normalize to the peak in the range xs to xe and plot
    nFiles = size( ROIdataStruct, 2 );
    nROI = size( ROIdataStruct(iFile).data.pensubdf, 2 );
    name = ROIdataStruct(iFile).name;
    xdata = ROIdataStruct(iFile).xdata;
    ydata = ROIdataStruct(iFile).data.pensubdf;
    npnts = size( ydata, 1 );
    ixs = find(xdata == xs);
    ixe = find( xdata == xe );
    range =  [ixs, ixe];

    ydataRange = zeros(1+ixe-ixs, nROI); %ydata( ixs:ixe, : );
    ydataSmooth = zeros(npnts, nROI); %ydata( ixs:ixe, : );
    for iROI = 1:nROI
        ydataSmooth(:,iROI) = smooth( ydata(:,iROI), nsmooth );
    end
    ydataRange = ydataSmooth( ixs:ixe, :);
    npnts = size(ydataRange,1);
    xdataRange = xdata( 1, ixs:ixe );
    xdataRange = reshape( xdataRange, npnts, 1 );
    threshIndex = NaN(nROI,1);
    threshTime = NaN(nROI,1);
    for iROI = 1:nROI
        [ymax, imax] = max( ydataRange(:,iROI) );
        ydataRange(:,iROI)=ydataRange(:,iROI)/ymax;
        tI = find( ydataRange(1:imax, iROI) < thresh, 1, 'last' );
        if ~ismissing(tI)
            threshIndex(iROI,1) = tI;
            threshTime(iROI,1) = xdataRange(tI,1);
        end
    end
    str = append( num2str(iFile),' - ',name);
    h = figure( 'Name', str );
    sh = plot( xdataRange, ydataRange);

end