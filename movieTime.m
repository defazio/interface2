function mov = movieTime( stack )
% 8 bit conversion
stack8b = dbl2uint8( stack );
% convert to multiframe array
MF8b = stack2MFarray( stack8b );
[MF8bmin, MF8bmax] = minmax(MF8b)

% write movie
%movname = name;
%success = writevideo( MF8b, movname);
mov = immovie( MF8b, jet );

end