% read the ROIpens.mat files stored in directory
% return data structure containing all data, plus summary data

%function outStruct = loadROIpens( localpath )

localpath = 'fresh roipens';
folderInfo = dir; % dir is a matlab variable containing current directory
currentFolder = folderInfo(1).folder;

% get the list of file names
upath = userpath; % default user directory "MATLAB" contains data directory "ROI data"

% assumes local path is extension of userpath
dpath = append(upath,localpath); % code for ROI stacks output to Igor
nfiles = size(folderInfo,1); % note first two are '.' and '..'
%%
nresults = nfiles -2;
peakF0 = nan(nresults,30);
baseF0 = nan(nresults,30);
for i=1:nresults
    dataName = folderInfo(i+2).name;
    fullpathname = append(folderInfo(i+2).folder, '/', folderInfo(i+2).name);
    S = load( fullpathname ); %, 'pensubdf' ); %ROIdataName, 'pensubdf');
    
    Y0 = S.ROIpens.fitresults(1,:); % each are hardcoded for now
    A1 = S.ROIpens.fitresults(2,:);
    A2 = S.ROIpens.fitresults(4,:);
    Bintercept = S.ROIpens.fitresults(7,:);
    nROIs = size(Y0,2);
    baseF0(i,1:nROIs)=Bintercept(1,:); % from linear fit
    peakF0(i,1:nROIs)=Y0(1,:)+A1(1,:)+A2(1,:); % from dbl exp fit

    names(i,1)={S.ROIpens.baseName};
    groups(i,1)={S.ROIpens.group};
end
results.baseF0=baseF0;
results.peakF0=peakF0;
d = dir(currentFolder);
%end