% plot a pixel over time
function plotPro( f, s1, s2, s3 )
frames = size( s1, 2 );
p1 = zeros( frames, 3 );
p1(:,1) = s1(:,2);
% p2 = zeros(frames);
p1(:,2) = s2(:,2);
p1(:,3) = s3(:,2);
figure(f)
clf
hold on
plot(s1)
plot(s2)
plot(s3)
hold off
end