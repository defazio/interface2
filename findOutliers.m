function out = findOutliers( A )
    tic
    sx = size( A, 1 );
    sy = size( A, 2 );
    sz = size( A, 3 );
    out = zeros( sx, sy, sz, 'logical' );
    for i = 1 : sx 
        for j = 1 : sy
            v = double( A( i, j, : ) );
            TF = isoutlier( v ); %, 'linear' );
            out(i,j,:) = TF;
        end
    end
    disp(append('outliers...', num2str(toc)));
end
