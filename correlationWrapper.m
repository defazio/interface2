function [ C, L, T, P ] = correlationWrapper( )
%Specify cross-correlation parameters:
%-------------------------------------
sample_frequency = Fs; % 20; %50e3; %sample frequency in Hz
time_window = 5; %time window length in seconds
time_step = dx; % 0.05; %time step in seconds
max_lag_time = 5; %maximum lag time in seconds

%Generate a sample signal:
%---------------------------
    iFile = 9;
    iROI1 = 9;
    iROI2 = 10;
    arr = ROIdataStruct( iFile ).data.pensubdf;
    xdata = ROIdataStruct( iFile ).xdata;
    normflag = 1;

    input1 = arr( : , iROI1 );
    input2 = arr( : , iROI2 );

%Process sample signals using timewindow_xcorr.m:
%------------------------------------------------

%No normalization: lag_time is 1 x nComp, twin is time window (x-axis), xcl
%is cross correlation timePoints x nComp
%-----------------
[ lag_time, twin, xcl ]=...
    timewindow_xcorr( input1, input2, sample_frequency,...
        time_window, time_step, max_lag_time, normflag);
