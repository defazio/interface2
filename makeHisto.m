% make peaks histogram/s
function h = makeHisto( S )%, minh, maxh, nbins )
% S is struct from imaging analysis
    minh= 0;
    maxh=2;
    %nbins = 20;
    delta = 0.1; 
    edges= minh:0.1:maxh;
    nFiles = max(size(S));
    for iFile = 1:nFiles
        nROIs = max(size(S(iFile).roi));
        figure
        for iROI = 1:nROIs
            arr = S(iFile).roi(iROI).pks;
            h.hi(iFile,iROI) = histogram(arr,edges);
            hold on
        end
    end
    figure
    hold on
    for iFile = 1:nFiles
        nROIs = max(size(S(iFile).roi));
        arrr = S(iFile).allPeaks;
        h.hr(iFile) = histogram(arrr,edges);
    end
end