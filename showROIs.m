%% structure ROIs derived from click or previous ROIs
function S = showROIs( hROI, ifig, im )
% hROI is a set of ROI handles
% ifig figure handle
% im image handle

r=10;
offset = 4;
[FOO, nROIs] = size(S_baseline.hROI);

for i=1:nROIs
    ROI1_center =  mean( S_baseline.hROI(i).ROI ) ; % get the center of the round ROI
    % revise the ROI
    hROI1rev = ROIfromClick3(ROI1_center(1),ROI1_center(2),S_baseline.maxp, ifig_baseline, r, offset );
    handles(i) = hROI1rev; 
end
% clim('auto')
% ifig_treat = figure;
% im_treat = imshow(S_treat.maxp, InitialMagnification='fit');
% clim('auto')