clc,clear
load('../stack.mat')
% Calculate the derivative, set the threshold, and filter outliers
% Take the partial derivative of time
[ outliers, derivativeMax ] = derivativeScreening(stack,137);
% Find the maximum value
[ outliers1, projMax1 ] = findOutliersMAX( stack, 4 ); % 4.5 x std threshold
% Find shift outliers on the x-y planes for the entire time period
[ outliers2,minA ] = shiftOutliers(stack,3000);    % Find pixels above 3700
length(find(outliers2==1))


