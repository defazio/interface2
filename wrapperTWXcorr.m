function [ rr, l, t ] = wrapperTWXcorr( xx, k, Fs, maxlag, frac_overlap )
% timewindow_xcorr wants time, not points
% meets general format of C, L, T output; correlation, lag and time stamps

% xx is nTimePoints x nROIs double
% k is in points
% Fs is sampling frequency, samples per second
% maxlag is in points
% frac_overlap is the fraction of window to overlap adjacent windows

% outputs: 
% rr is correlation (nlags x nTimePoints x ncomparisons)
% l is lag in time units (nlags)

% % take a breath
% gonna compare each ROI to each ROI once, over the window k, order dos
% output is linearized, 12 13 14 1... 1n 23 24 2... 2n etc

    [ nTimePoints, nROI ] = size( xx );
    window = k; % points used for window
    time_window = k / Fs; % the duration of the window in seconds
    time_step = time_window * frac_overlap; % this is the space between window centers, seconds
    maxlag_time = maxlag/Fs;
    normflag = 1;

    noverlap = floor( frac_overlap * window ); % number of overlapping points
    nC = commute( nROI ); % brute force calculation of number of comparisons
    nLags = 2 * maxlag + 1;
    
    %nr = fix( (nTimePoints - noverlap) / (window - noverlap) ); % number of correlations, time points
    nr = nTimePoints; % hardcoding time_step = 1/Fs
    %rr = NaN( nLags, nr, nC );
    %ll = NaN( nLags );
    
    % comparisons
    index = 1; % comparison index
    for m = 1:nROI
        for n = (m+1):nROI
            x = xx( :, m );
            y = xx( :, n );
            % run the corrgram for this combo
    
            %[ C, L, T ] = migram( x, y, maxlag, window, noverlap );
   [ lag_time, twin, xcl ] = timewindow_xcorr( ...
       x, y, Fs, time_window, time_step, maxlag_time, normflag);

            rr( :, :, index ) = xcl';
            
            index = index + 1;
        end
    end
    l = lag_time; %  / Fs; % lags in time units
    t = twin; % / Fs; % time in time units
end