function mov = movieTimeBurnIn( stack, ROIs )
% ROIs, array of 8x2 polygon vertices

% 8 bit conversion
stack8b = dbl2uint8( stack );

% convert to multiframe array
MF8b = stack2MFarray( stack8b );
stackBurn = insertShape( MF8b, "polygon", ROIs );
% write movie
%movname = name;
%success = writevideo( MF8b, movname);
mov = immovie( stackBurn, jet );

end