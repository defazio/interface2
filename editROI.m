function hROI = editROI( imStack, hin )
% h contains handles for ROI polygons
% imstack is the raw data stack
    tic
    val = NaN;
    nROIs = size( hin, 2 ); % number of ROIs
    dz = size( imStack, 3 ); % number of frames
    maxProj = projectMax(imStack);

    %figure('Name','editROI')
    him = imshow(maxProj);
    %clim( 'auto' );
    axis image
    axis on
    hold on
    msize = 1; 
    lsize = 1;
    for j = 1 : nROIs % loop over each ROI, loop over frames is within

        penpoly = hin(j).handles.penumbra.Position;
        deadpoly = hin(j).handles.deadzone.Position;
        roipoly = hin(j).handles.ROI.Position;
        
        handles.penumbra = drawpolygon(gca,'Position', penpoly, 'LineWidth', lsize, 'MarkerSize', msize,  'Color', [1,0,1], FaceAlpha=0 );
        handles.deadzone = drawpolygon(gca,'Position', deadpoly, 'LineWidth', lsize, 'MarkerSize', msize, 'Color', [1,1,1], FaceAlpha=0 );
        handles.ROI = drawpolygon(gca,'Position', roipoly, 'FaceAlpha', 0, 'LineWidth', lsize, 'MarkerSize', msize );

        hROI(j).handles.ROI = handles.ROI;
        hROI(j).handles.deadzone = handles.deadzone;
        hROI(j).handles.penumbra = handles.penumbra;

    end
    % stolen from https://www.mathworks.com/matlabcentral/answers/724138-real-time-editable-roi-with-mask?s_tid=srchtitle
    for j=1:nROIs
        h = hROI(j).handles.ROI;
        l1 = addlistener(h,'ROIMoved',@(src,evt)ROIMovedCallback(src,evt,j,hROI)); %this listener I have added to listen whenever we move ROI
    end
end

function ROIMovedCallback(src,evt,index,hROIarray)
disp(index)
%src
%evt
% c = createMask(b);
% cc = c.*imdata;
% d = sum(cc,[1,2]);
% disp(d); %displaying the values of d
end