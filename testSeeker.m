% return FFA dF over F0
%function FFAdFoF0 = returnFFA( datecode )
path = '/Volumes/Extreme SSD/processed/complete';
data = 'base';
datecode ='20211111c21';
tdExtension = 'complete';
extension = '.mat';
fname = append(datecode,tdExtension,extension);
base = seeker(path, fname, data);
FFAbase = mean(base,[1 2]);
