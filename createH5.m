% move to h5 format for processing: setup h5 based on crop
function createH5( stack, name, tag )
tic
height = size( stack, 1);
width = size( stack, 2);
frames = size(stack, 3);
fullname = append( name, '.h5' );
h5create( fullname, tag, [height width frames], 'datatype', class(stack));
h5write( fullname, tag, stack );
h5disp( fullname );
disp(append('create and write HDF5: ', num2str(toc), ' secs' ));
end