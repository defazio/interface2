% SCRIPT FOR CORRELATION ANALYSIS
%   note runs on entire ROIdataStruct, contains multiple experiments

<<<<<<< HEAD
=======

>>>>>>> parent of 9ec7d79 (working on revising ROI)
%group = 'male intact';
group = ["male sham intact" "male sham intact" "ORX" "ORX"];
pdfname = append('corr.pdf');
Fs = 1/dx; %10; %20; % 20 hz

%corr params in seconds
window_time = 2; % seconds
%maxlag_time = 0.5; % seconds
%frac_overlap = 0.95; % overlap fraction
disp('started correlation analysis...')
CorrLagStore = CorrProcessing( ROIdataStruct, ...
    Fs, window_time ); %, maxlag_time, frac_overlap );
disp('completed correlation analysis...')

%%
set(0,'DefaultFigureWindowStyle','docked')
for iFile=1:nFiles
    xend =  35; %max(ROIdataStruct(iFile).xdata)/60; % * 0.2 ; %seconds! 600; %
    xstart = 0;
    titleStr = append(group(iFile), ': ', num2str(iFile),...
        ' ',ROIdataStruct(iFile).name);
    h = figure('Name',titleStr, 'NumberTitle','off');
    tiledlayout(5,1)

    fighandle.h = h;
    fighandle.ax0 = nexttile([2,1]);

    arr = ROIdataStruct(iFile).data; %.pensubdf; % ntimepoints x nROIs
    xdata = ROIdataStruct(iFile).xdata/60; % convert to minutes
    % plot ROI dFoF0
    plot(xdata, arr)
    title(titleStr)
    hold on
    nROI = size( arr, 2 );
    for iROI = 1: nROI
        % mark the t50 rise times
        %  times = analytes.File(iFile).ROI(iROI).crossings/60; % minutes
        %  crossings = analytes.File(iFile).ROI(iROI).crosslev;
        %  scatter( times, crossings, 'o' )
        % mark the peaks
        times = analytes.File(iFile).ROI(iROI).times/60;
        % minutes
        pks = analytes.File(iFile).ROI(iROI).peaks;
        % plot detected peaks
        scatter( times, pks, 'o', 'filled' )
    end
    str=append('ROI dF/F0 ');
    ylabel(str)
    hold off

% raster
    fighandle.ax1 = nexttile;
    nofigure =1;
    hTDR = TDrasterIndex(analytes,iFile,nofigure);
    str=append('ROIs');
    ylabel(str)
% time histo
    % ax2 = nexttile;
    % combine all data for each file
    % binsize = 5; % seconds
    % events = linearize( analytes );
    % ts = events(iFile,:);
    % [ bindata(iFile,:), hTSH ] = timeSeriesHistogram( ts, binsize, xstart, xend );

% FFA = full frame average, convert imaging data to bulk fiber    
    fighandle.ax3 = nexttile;

    plot(xdata, ROIdataStruct(iFile).FFA)
    str=append('frame dF/F0 ');
    ylabel(str)

% plot lag
    fighandle.ax4 = nexttile;

    C = CorrLagStore(iFile).C; % = C;
    rrmaxmean = CorrLagStore(iFile).Cmaxmean; % = rrmaxmean;
    % plot correlation mean
    p = plot(xdata, rrmaxmean );
    xlim([xstart xend])
    str=append('corr ', num2str(window_time), 'sec win');
    ylabel(str)
    xlabel 'time (minutes)'

    linkaxes([fighandle.ax0,fighandle.ax1,fighandle.ax3,fighandle.ax4], 'x' )

    exportgraphics(h,pdfname,'Append',true)
end % loop over files
%% plot raster for all files in one figure
set(0,'DefaultFigureWindowStyle','docked')
titleStr = append(num2str(iFile),' ',ROIdataStruct(iFile).name);
h = figure('Name','raster plot', 'NumberTitle','off');
tiledlayout(nFiles,1)

for iFile=1:nFiles
    titleStr = append(group(iFile), ': ', num2str(iFile),...
        ' ',ROIdataStruct(iFile).name );
    xend =  max(ROIdataStruct(iFile).xdata)/60; % * 0.2 ; %seconds! 600; %
    xstart = 0;

    arr = ROIdataStruct(iFile).data; %.pensubdf; % ntimepoints x nROIs
    xdata = ROIdataStruct(iFile).xdata/60; % convert to minutes

    % raster
    fighandle.ax(iFile) = nexttile;
    nofigure =1;
    hTDR = TDrasterIndex(analytes,iFile,nofigure);
    title(titleStr);
    h2=gca;
    axis off
    %h.XAxis.Visible='off';
    str=append(ROIdataStruct(iFile).name);
    ylabel(str)
    h2.YAxis.Visible='on';
    h2.FontSize=10;
    h2.YAxis.Exponent=0;
    ytickformat('%3.0f')
    h2.TickDir='out';
    h2.TickLength=[0.00200 0.0020];
    if iFile==nFiles
        h2.XAxis.Visible='on';
        str = 'time (minutes)';
        xlabel(str);
        linkaxes(fighandle.ax); %, 'x' );
        exportgraphics(h,pdfname,'Append',true)
    end
end % loop over files
% h.XAxis.Visible='on';
% str = 'time (minutes)';
% xlabel(str);
% linkaxes(fighandle.ax); %, 'x' );
   %% plot full frame average for all files in one figure
set(0,'DefaultFigureWindowStyle','docked')
titleStr = append(num2str(iFile),' ',ROIdataStruct(iFile).name);
h = figure('Name','full frame average', 'NumberTitle','off');
tiledlayout(nFiles,1)

for iFile=1:nFiles
    titleStr = append(group(iFile), ': ', num2str(iFile),...
        ' ',ROIdataStruct(iFile).name );

    xend =  max(ROIdataStruct(iFile).xdata)/60; % * 0.2 ; %seconds! 600; %
    xstart = 0;

    arr = ROIdataStruct(iFile).data; %.pensubdf; % ntimepoints x nROIs
    xdata = ROIdataStruct(iFile).xdata/60; % convert to minutes

    fighandle.ax(iFile) = nexttile;
    % nofigure =1;
    % hTDR = TDrasterIndex(analytes,iFile,nofigure);

% FFA = full frame average, convert imaging data to bulk fiber    

    plot(xdata, ROIdataStruct(iFile).FFA)
    title(titleStr);
    h2=gca;
    axis off
    %h.XAxis.Visible='off';
    str='full frame average, dF/F0'; %append(ROIdataStruct(iFile).name);
    ylabel(str)
    h2.YAxis.Visible='on';
    h2.FontSize=10;
    h2.YAxis.Exponent=0;
    ytickformat('%3.3f')
    h2.TickDir='out';
    h2.TickLength=[0.00200 0.0020];

    if iFile==nFiles
        h2.XAxis.Visible='on';
        str = 'time (minutes)';
        xlabel(str);
linkaxes(fighandle.ax, 'x' );
        exportgraphics(h,pdfname,'Append',true)
    end
end % loop over files
% h.XAxis.Visible='on';
% str = 'time (minutes)';
% xlabel(str);

%% time series histogram
set(0,'DefaultFigureWindowStyle','docked')
iFile=1;
titleStr = append(group(iFile), ': ', num2str(iFile),...
        ' ',ROIdataStruct(iFile).name);
h = figure('Name','time series histogram', 'NumberTitle','off');
tiledlayout(nFiles,1)
binsize = 10/60; % now minutes % seconds
binValues = zeros(nFiles,400);
for iFile=1:nFiles
    titleStr = append(group(iFile), ': ', num2str(iFile),...
        ' ',ROIdataStruct(iFile).name, '; bin ',num2str(binsize*60), ' sec' );

% time series histogram
    fighandle.ax(iFile) = nexttile;
    
    ts = events(iFile,:)/60; % convert seconds to minutes
    xstart = 0; 
    xend = max(ts);
    [bins, histo]=timeSeriesHistogram( ts, binsize, xstart, xend );
    hsize = size( histo.Values );
    binValues( iFile, 1 : hsize(2) ) = histo.Values( 1, : );
    title(titleStr);
    h2=gca;
    axis off
    str=append('events per ',num2str(binsize*60), 's');%append(ROIdataStruct(iFile).name);
    ylabel(str)
    h2.YAxis.Visible='on';
    h2.FontSize=10;
    h2.YAxis.Exponent=0;
    ytickformat('%3.0f')
    h2.TickDir='out';
    h2.TickLength=[0.00200 0.0020];
    if iFile==nFiles
        h2.XAxis.Visible='on';
        str = 'time (min)';
        xlabel(str);
        linkaxes(fighandle.ax); %, 'x' );
        ylim([0 40])
        exportgraphics(h,pdfname,'Append',true)
    end
end % loop over files

