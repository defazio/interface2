function [ ydataRange ] = superimposePlotsArr( ...
    xdata, ydata ) %, xs, xe, nsmooth, nofigure )

% xdata: 1 x ntimepoints
% ydata nROI x ntimepoints
% xs, xe plot range

% normalize to the peak in the range xs to xe and plot

    npnts = size( ydata, 1 );
    ixs = find(xdata == xs);
    ixe = find( xdata == xe );
    range =  [ixs, ixe];

    ydataRange = zeros(1+ixe-ixs, nROI); %ydata( ixs:ixe, : );
    ydataSmooth = zeros(npnts, nROI); %ydata( ixs:ixe, : );
    for iROI = 1:nROI
        ydataSmooth(:,iROI) = smooth( ydata(:,iROI), nsmooth );
    end
    ydataRange = ydataSmooth( ixs:ixe, :);
    npnts = size(ydataRange,1);
    xdataRange = xdata( 1, ixs:ixe );
    xdataRange = reshape( xdataRange, npnts, 1 );
    threshIndex = NaN(nROI,1);
    threshTimes = NaN(nROI,1);
    for iROI = 1:nROI
        [ymax, imax] = max( ydataRange(:,iROI) );
        ydataRange(:,iROI)=ydataRange(:,iROI)/ymax; % normalize 
        % this should be optional: find the threshold time
        % tI = find( ydataRange(1:imax, iROI) < thresh, 1, 'last' );
        % if ~ismissing(tI)
        %     threshIndex(iROI,1) = tI;
        %     threshTimes(iROI,1) = xdataRange(tI,1);
        % end
    end
    str = append( num2str(iFile),' - ',name);
    if(~nofigure)
        h = figure( 'Name', str );
    end
    sh = plot( xdataRange, ydataRange);

end