% return FFA dF over F0
function [base, hROI, timing] = returnHandlesBase( datecode )
path = '/Volumes/Extreme SSD/processed/complete';
tdExtension = 'complete';
extension = '.mat';
fname = append( datecode, tdExtension, extension );
base = seeker( path, fname, 'base' );
hROI = seeker( path, fname, 'hROI' );
timing = seeker( path, fname, 'basetiming' );
end
