function [order] = orderFromStruct( times, CE )
% times should be an array of event times times( nCEs, nEvents )
% CE is a structure containing the timing info (and ROI and event) for a
% single experiment
% CE( iCE, iCEe ).time, roi, event; iCEe is event within iCE
% iCE is an increment over the coordinated events identified in the
% file/exp
[ nCEs, maxEvents ] = size( times );
%indexes = NaN(maxEvents,1);
%timesCheck = NaN( nCEs, maxEvents );
order = NaN(nCEs,maxEvents,2);
for iCE = 1 : nCEs
    etimes =  times( iCE, : );
    [ sorted, indices ] = sort(etimes);
    % indices should contain temporal order of ROIs
    for index = 1 : maxEvents
        %timesCheck(index,1) = CE( iCE, indices(index), 1 ); % first position is the time of event
        %indexes(index,1) = CE( iCE, indices(index), 2 ); % second position is ROI index
        order( iCE, index, 1 ) = CE( iCE, indices(index), 1 );
        order( iCE, index, 2 ) = CE( iCE, indices(index), 2 );
        
    end
end
end
