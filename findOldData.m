% import data from *_Igor.mat files (contains ROI data)
function findOldData(  )

localpath = '/ROI data/*_Igor.mat'; % original data set 10min, NOT FULL PATH!

%disp('WARNING! VERIFY XDATA!')
folderInfo = dir;
currentFolder = folderInfo(1).folder;

% get the list of file names
upath = userpath; % default user directory "MATLAB" contains data directory "ROI data"
dpath = append(upath,localpath); % code for ROI stacks output to Igor
ROIdirStruct = dir(dpath);
nROIstacks = size(ROIdirStruct,1);
for i=1:nROIstacks
     ROIdataName = ROIdirStruct(i).name;
     ROIdataStruct(i).name = ROIdataName(1:end-9); % removes '_Igor.mat' from file name
     fullpathname = append(ROIdirStruct(i).folder, '/', ROIdirStruct(i).name);
     S = load( fullpathname ); %, 'pensubdf' ); %ROIdataName, 'pensubdf');
     if ~isfield(S,'timing')
        updateOldData( ROIdataStruct(i).name );
     else
         disp('did not update: ',fullpathname)
     end

end
% d = dir(currentFolder);
end