function S = td_moco2(Yraw)
% modified from normCorr demo.m
% removed non-rigid motion correction, refer to original demo.m for info
% creates a shit ton of variables, clean up?
Yraw = single(Yraw);
[d1,d2,T] = size(Yraw);
% FILTER: original normCorr demo settings, heavy filtering
    gSig = 7; 
    gSiz = 3*gSig; 
    psf = fspecial('gaussian', round(2*gSiz), gSig);
    ind_nonzero = (psf(:)>=max(psf(:,1)));
    psf = psf-mean(psf(ind_nonzero));
    psf(~ind_nonzero) = 0;   % only use pixels within the center disk
    %Y = imfilter(Yraw,psf,'same');
    %bound = 2*ceil(gSiz/2);
    disp('imfilter upgrade to imgaussfilt?')
    tic; Y = imfilter(Yraw,psf,'symmetric'); toc;
    disp('imfilter complete')
    bound = 0;
% MOTION CORRECTION PARAMS: first try out rigid motion correction
    % exclude boundaries due to high pass filtering effects
options_r = NoRMCorreSetParms('d1',d1-bound,'d2',d2-bound,'bin_width',200,'max_shift',20,'iter',1,'correct_bidir',false);

% MOTION CORRECTION: register using the high pass filtered data and apply shifts to original data
tic; [M1,shifts1,template1] = normcorre_batch(Y(bound/2+1:end-bound/2,bound/2+1:end-bound/2,:),options_r); toc % register filtered data
    % exclude boundaries due to high pass filtering effects
tic; Mr = apply_shifts(Yraw,shifts1,options_r,bound/2,bound/2); toc % apply shifts to full dataset
    % apply shifts on the whole movie
% SHOW YOUR WORK: compute metrics 
[cY,mY,vY] = motion_metrics(Y(bound/2+1:end-bound/2,bound/2+1:end-bound/2,:),options_r.max_shift);
[cYraw,mYraw,vYraw] = motion_metrics(Yraw,options_r.max_shift);

[cM1,mM1,vM1] = motion_metrics(M1,options_r.max_shift);
[cM1f,mM1f,vM1f] = motion_metrics(Mr,options_r.max_shift);

% plot rigid shifts and metrics
shifts_r = squeeze(cat(3,shifts1(:).shifts));
% figure;
%     subplot(311); plot(shifts_r);
%         title('Rigid shifts','fontsize',14,'fontweight','bold');
%         legend('y-shifts','x-shifts');
%     subplot(312); plot(1:T,cY,1:T,cM1);
%         title('Correlation coefficients on filtered movie','fontsize',14,'fontweight','bold');
%         legend('raw','rigid');
%     subplot(313); plot(1:T,cYraw,1:T,cM1f);
%         title('Correlation coefficients on full movie','fontsize',14,'fontweight','bold');
%         legend('raw','rigid');
% disp('td_moco complete!')
S.shifts_r = shifts_r;
S.T = T;
S.cY = cY;
S.cM1 = cM1;
S.cYraw = cYraw;
S.cM1f = cM1f;
S.Mr = Mr;

end