





% DEAD CODE DO NOT USE







% ROI selection starts here!
function [ handle ] = click2ROIman( im ) %, radius ) %, stack )
% image for user to click, radius is the search radius in pixels

% OLD function [handles,stats] = click2ROI( im, stack )
% returns handles for up to 10 ROI
% im is an image you want to use for picking ROIs
% old -- stack is the image stack you want to analyze with the ROI
tic
%df = figure; % target for data plot
% imgf = figure; % ( 'ButtonDownFcn', @figCallBackFcn );
% imgo = imshow( im );
% clim( 'auto' );
% axis image
% axis on
% hold on
n=1; % number of ROIs to generate
F = getframe; % grab the current frame from movie
npoints = 8;
for i=1:n
    coords = NaN( npoints, 2 );
    [x,y]=ginput( npoints ); % ginput returns native coordinates of click
    drawline(x,y)
end


toc
end
