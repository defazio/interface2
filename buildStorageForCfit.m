% concatenate coeffs from linear and dbl fits

catCoeffnames = cat( 1, coeffnames(f), coeffnames(lfit) )
catCoeffvals = cat( 2, coeffvalues(f), coeffvalues(lfit) )