function dispROIstack2(ROIstack)
n = size( ROIstack, 2 ); % number of ROIs

for i=1:n % loop over ROIs to plot the processed data
    trace = ROIstack(:,i);
    figure('name',num2str(i));
    hold on
    smth = boxcar( trace , 11 );
    plot( smth ); 
    SD = std( trace );
    yline(0.5*SD)
    yline(0.25*SD)

    yyaxis right
    dx = 0.1; % sec
    prewin = 11; % boxcar smooth window
    postwin = 21;
    der = diffSmooth( trace, dx, prewin, postwin );
    SDder = std( der );
    plot( der )
    yline(SDder)

end

end