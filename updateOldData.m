function updateOldData( datecode )
    disp( append( 'updating old data: ', datecode ) );
    
    basename = datecode;
    [ base, hROI, basetiming ] = returnHandlesBase( basename ); % pulls data from large "complete.mat" files
    
    disp(append('starting roi processing: ',datecode));
    figure('Name', datecode );
    baseStats = roi2stats3D2( base, hROI ); % uses "raw" (cropped, smoothed etc)
    baseStats.timing = squeeze( basetiming );
    disp('done with ROI stats')

    % process ROI time course data for dF/F0
    %  appends dF/F0 data to baseStats structure
    disp('starting roi dFoF0')
    baseStats = processROIs( baseStats );
    displayROIs( baseStats );
    disp('done with roi dFoF0')
    
    % EXPORT TO IGOR and MATLAB
    fname = append( basename, '_Igor');
    export2igor( baseStats, fname ) % custom function for export ROI data
    % save entire base stats structure
    fname = append( basename, '_baseStats');
    save(fname, 'baseStats')
    disp('export of Igor data complete')
    
    % saves entire workspace, big file
    filename = append( basename, 'complete.mat');
    save(filename,'-v7.3')
    disp('saved: ', filename, ' please move data files to proper locations!')

end