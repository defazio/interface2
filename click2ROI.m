% ROI selection starts here!
function [ handles ] = click2ROI( im, imf, radius, offset ) %, stack )
% image for user to click, radius is the search radius in pixels
    n=20;
    clear handles
    for i=1:n
        [x,y]=ginput(1);
        if size([x,y])>0
            handles(i) = ROIfromClick( y, x, im, imf, radius, offset );
        else
            break
        end
    end
end
