function [out, minA] = shiftOutliers(A,maxThreshold)
%SHIFTOUTLIERS 
%  maxThreshold: if the value exceeds it, the value is abnormally large

[n,m,k] = size(A);
minA = zeros(n,m);

% For each pixel, find the minimum value in the time period
for i = 1:n
    for j = 1:m
          minA(i,j) = min(A(i,j,:));
    end
end
% Find the pixel whose value is larger than the other pixels
out = minA > maxThreshold;
% find(out==1)
end




