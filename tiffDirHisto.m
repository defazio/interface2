% import data from *_Igor.mat files (contains ROI data)
function S = tiffDirHisto( localpath )

    folderInfo = dir;
    currentFolder = folderInfo(1).folder;
    
    % get the list of file names
    upath = userpath; % default user directory "MATLAB" contains data directory "ROI data"
    dpath = append(upath,localpath); % code for ROI stacks output to Igor
    dirStruct = dir(dpath);
    nfiles = size(dirStruct,1);
    S.dir = dirStruct;
    %figure
    set(0,'DefaultFigureWindowStyle','docked')
    radius = 8;% pixels to search for half amplitude, use 2 pixel if spatial resize
    offset = 2;% pixel separation between roi, deadzone, penumbra
    
    for i=1:nfiles % skips ., .., and dstore
        fname = dirStruct(i).name
        if(strlength(fname)>10)
            S.Image(i).fname = fname;
            fullpathname = append( dirStruct(i).folder, '/', dirStruct(i).name);
            S.Image(i).img = imread( fullpathname);
            titleStr = fname;
            S.Image(i).fh = figure( 'Name',titleStr, 'NumberTitle','off');
            S.Image(i).ih = imshow( S.Image(i).img );
            clim('auto')
            % click2ROI2 is fixed size ROIs, independent of x-y profile 
            %hROI = click2ROI2( maxDFoF0, radius, offset ); % handles contains info on ROIs
            S.Image(i).hROI = click2roundROI( S.Image(i).img, S.Image(i).fh, radius, offset ); % handles contains info on ROIs
            %nROIs = size( hROI, 2 );
            %edges = 0 : 100 : 60000;
            %h=histogram(S.Image(i).tiff, edges );
            %S.bins(i,:)=h.Values;
        end
    end
    d = dir(currentFolder);
end