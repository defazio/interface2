% ROI selection starts here!
function [ handles ] = click2ROI3( im1, im2, radius, offset ) %, stack )
% use composite image
% image for user to click, radius is the search radius in pixels

% returns handles for up to 10 ROI
% im is an image you want to use for picking ROIs
tic
%df = figure; % target for data plot
imgf = figure; % ( 'ButtonDownFcn', @figCallBackFcn );
im1h = histeq(im1);
im2h = histeq(im2);
imgo = imshowpair( im1, im2 );
clim( 'auto' );
axis image
axis on
hold on
n=20;
clear handles
for i=1:n
    % manual ROI, auto deadzone, penumbra
    h = manROI(im1, radius, offset);
    if isstruct(h)
        handles(i) = h;
    else
        break
    end
    % [x,y]=ginput(1);
    % if size([x,y])>0
    %     handles(i) = ROIfromClick( y, x, im, imgf, radius, offset );
    %     %stats(i) = roi2stats3D( stack, handles(i) );
    % else
    %     break
    % end
end

toc
end
