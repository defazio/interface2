function [ rr, rrmean ] = corrRunner( arr, Fs, window_time ); %, maxlag_time ) %, frac_overlap )
% handles all the calculations for moving correlation with lag
% wrapper for the wrapper for movcorr -- NOT "corrgram.m" Norwan!

    %corr params
    k = fix(window_time * Fs); % corr window in points
   
    [ rr, pp ] = wrapperMovCorr( arr, k ); %, Fs, maxlag, frac_overlap );
     % MovCorr doesn't test lag
     
    rrmean = mean( rr, 2 ); % this is "overall correlation" as a function of time
    % mean is taken over all pairs

end
