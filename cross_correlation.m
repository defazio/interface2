load('eventDB.mat'); % load eventDB variable from the file
numROIs = size(eventDB, 1); % get the number of ROIs
corrMat = ones(numROIs); % initialize the correlation matrix with ones

% loop over all pairs of ROIs and calculate their cross-correlation
for i = 1:numROIs
    eventTimes1 = eventDB(i, 2:end); % extract the event times for ROI i
    eventTimes1( isnan(eventTimes1) ) = [];
    nEvents1 = length(eventTimes1); % get the number of events in ROI i
    for j = i+1:numROIs
        eventTimes2 = eventDB(j, 2:end); % extract the event times for ROI j
        eventTimes2( isnan(eventTimes2) ) = [];
        nEvents2 = length(eventTimes2); % get the number of events in ROI j
        [corr, lags] = xcorr(eventTimes1, eventTimes2, 'none'); % calculate the cross-correlation
        max_corr = max(corr); % get the maximum correlation value
        norm_factor = sqrt(nEvents1 * nEvents2); % calculate the normalization factor
        corrMat(i,j) = max_corr / norm_factor; % store the normalized correlation value in the matrix
        corrMat(j,i) = max_corr / norm_factor; % store the normalized correlation value in the matrix (symmetric)
    end
end

% display correlation matrix
figure;
imagesc(corrMat);
colorbar;
title('Cross-Correlation Matrix');
xlabel('ROI');
ylabel('ROI');