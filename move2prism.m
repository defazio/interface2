function move2prism(S)

maxevents=1000;
nfiles = max(size(S));
for ifile = 1: nfiles
    nrois = max(size(S(ifile).roi));
    arr = nan(nrois,maxevents);
    for iroi=1:nrois
        nevents = size(S(ifile).roi(iroi).pks,1);
        arr(iroi,1:nevents) = S(ifile).roi(iroi).pks;
    end
    name = append('S',num2str(ifile),'.roi',num2str(iroi),'.csv');
    csvwrite(name,arr);

end