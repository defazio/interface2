% getting FFA from "complete"
%localpath = '/EGTA/*_baseStats.mat';
%localpath = '/ROIdataLT/orx/*_baseStats.mat';
localpath = '/baseStats/*_baseStats.mat';
%localpath = '/ROI data/*_Igor.mat'; % original data set 10min
%localpath = '/ROI data 5 min/*_Igor.mat'; % original data set 10min
%localpath = '/EGTA/*_Igor.mat';
% check if older files missing variables

% reprocess older files

% proceed
% 20231130 this function is not currently returning FFA
% get raw returns a struct containing *.ROI.meanZ
% for each file returned by localpath
ROIpens = getRAW(localpath);