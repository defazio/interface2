function cleaned = replaceOutliers( A, outliers )
    % out is a logical array identifying outliers to limit search
    tic
    sx = size( A, 1 );
    sy = size( A, 2 );
    % sz = size( A, 3 );
    cleaned = A; %zeros( sx, sy, sz, 'uint16');
    %figure
   %v = zeros(1,sz);
    for i = 1 : sx 
        for j = 1 : sy
            if outliers(i,j) == 1
                % v = double( A( i, j, : ) );
                % vc = filloutliers( v, 'linear' );
                % suround mean from Eve
                meanround = zeros(1,size(A,3));
                x=i;
                y=j;
                disp(num2str(A(i,j,1)))
                for k = x-1:1:x+1
                    for l = y-1:1:y+1

                        if (k==x) && (l==y)
                        else
                            for m = 1:size(A,3)
                                % revise to match size limits
                                if (k>0)&&(l>0)&&(m>0)&&(k<=512)&&(l<=512)&&(m<=512)
                                    meanround(1,m) = meanround(1,m) + A(k,l,m);
                            
                                end
                            end
                            %disp(append("x: ", num2str(k)," y: ", num2str(l)," val: ", num2str(A(k,l,1))));
                        end
                    end
                end
                meanround = meanround/8; %replace outliers with the surrounding 8 pixels.
                cleaned( i, j, : ) = meanround; % uint16( vc );
                %plotMultiY(v,vc);
                %pause
            end
        end
    end
    disp(append('replace outliers...', num2str(toc)));
end
