% master
% open one dataset, preprocess, select ROIs

% run openETS, extracts image stack into memory
% get two files, baseline and treatment
disp("reading ETS and VSI...")
% rbase and rtreat raw data files, nbase ntreat meaningful names
[ baseRaw, baseTiming, baseInterval, baseName, baseETS, metadata ] = openETSui(); % puts image stack into 'stack'
%[ treatRaw, treattiming, treatName, treatETS ] = openETSui(); % puts image stack into 'stack'
disp("ETS read into memory...")
% timing = baseTiming;
% interval = baseInterval;

%% MOVIE TIME! assess movement
disp('make a movie')
movfilename = baseName; % append(baseName,".avi"); % ext is auto '.mj2'
V = VideoWriter(movfilename,'Grayscale AVI'); %'Archival');
open(V)
mov = movieTime( baseRaw );
implay( mov, 100 );
writeVideo( V, mov )
close(V)
disp('movie complete')
% %% MOVIE TIME
%%
% cuts off first 10s, 100 frames with fast transient
disp("trimming started...")
% base and treat are image stacks from previous step
trimSecs = 10; % seconds
trimPoints = 10 / (baseInterval/1000); % base interval is in milliseconds
[base0,baseTiming] = trimArray( baseRaw, baseTiming, 1, trimPoints );
%treat = trimArray( treatRaw, 3, 1, 100 );
disp(append("trimming completed...removed: ", num2str(trimPoints) ) );
%% if no trimming
base0 = baseRaw;
%% saves entire workspace, big file
filename = append( baseName, 'preclean.mat');
save(filename,'-v7.3')
disp('saved preclean')
%%
thresh =8;
% clean stack
disp('cleaning ')
% oBase is base outliers, pmBase is projectMaxBase
[ baseOutliers, baseProjMax ] = findOutliersMAX( base0, thresh ); % 4.5 x std threshold
%%
base = replaceOutliers( base0, baseOutliers );
disp("cleaning completed, check output!")
[ baseOutliers, baseProjMax ] = findOutliersMAX( base, thresh ); % 4.5 x std threshold
%%
thresh =3.1;
% clean stack
disp('cleaning ')
% oBase is base outliers, pmBase is projectMaxBase
[ baseOutliers, baseProjMax ] = findOutliersMAX( base, thresh ); % 4.5 x std threshold
%%
base = replaceOutliers( base, baseOutliers );
disp("cleaning completed, check output!")
[ baseOutliers, baseProjMax ] = findOutliersMAX( base, thresh ); % 4.5 x std threshold
%% if no outliers!
base = base0;
%%
% %% OPTIONAL gaussian spatial smoothing (xy plane filter for each frame)
% disp("spatGauss")
% %stack = spatGauss( stack, 0.5, 5 );
% disp("spatGauss completed")

%% run tdResize, bin stack, 4x4 again seems fine!
disp('resize, 4x4 binning')
bin = 2;

% STOPPED WORKING FOR BIG STREAMS!

base = tdResize( base, bin );

%treat = tdResize( treat, bin );
disp("spatial resize completed")
%% run cropstack - project max, cut as small as possible
disp('cropstack')
[ base, rect ] = cropStack( base ); % user must right click and crop
%[ treat ] = cropStackRect( treat, rect ); % applies same crop as base
disp("crop completed")
%% set timing if not resizing
timing=baseTiming;
%% OPTIONAL resize in time
disp('downsampling in time')
rsT = 2; % halve the data points
[base,baseT] = resizeZ( base, baseTiming, rsT ); % downsample by rsT frames
timing = baseT;
interval = mean(diff(baseT));
disp("downsampling completed")
%% save before dFoF0 crash
chapter1 = append(baseName,'PredFoF0.mat');
save(chapter1, '-v7.3')
disp(append('saved ',chapter1))
%% dF/F0: vulnerable to size of data set, no protection is place
% crash is dependent on available RAM
% clean out vars

disp('dFoF0')
t1=20; %was 50 for low activity 10s?, frames, 0.1 sec / frame right now!
t2=inf; % frames, should be 500s but MinBarF is really slow! 
t4=200; % frames to define a prominent event for slope fill
[ baseDFoF0, baseMinBF ] = deltaFoverFzeroEmbedded( base, t1, t2, t4 ); % includes t1 and t2 steps above!
disp('dFoF0 completed')

%% OPTIONAL smooth stack
%sDFoF0 = SGtStack( dFoF0, 3, 9 );

%% MOVIE TIME!
disp('make a movie')
movfilename = baseName; % append(baseName,".avi"); % ext is auto '.mj2'
V = VideoWriter(movfilename,'Archival');
open(V)
mov = movieTime( baseDFoF0 );
implay( mov, 100 );
writeVideo( V, mov )
close(V)

%%
%[ meanDFoF0, maxDFoF0, stdDFoF0 ] = projMeanMaxSTD(baseDFoF0);
figure
[ meanStack, maxStack, stdStack ] = projMeanMaxSTD(base);

%% save before ROI selection, maybe not necessary?
chapter1 = append(baseName,'dFoF0.mat');
save(chapter1, '-v7.3')
disp('saved')

%% SELECT ROIs
% NOTE: PROCESSES ONE EXPERIMENT AT A TIME
% master for ROI analysis
% assumes dFoF0 stack/movie is already generated 
%    (see master.m or load matlab.mat dataset)
% outline: 
%    get roi set (includes deadzone, penumbra), 
%    reprocess dF/F0 from "stack" (raw but includes crop, downsampling, etc)
%    display results
%    output to Igor? or process events here?

%% get ROIs from user
disp('PLEASE DO NOT CLOSE FIGURE! max 20 ROIs')
radius = 4;% pixels to search for half amplitude, use 2 pixel if spatial resize
offset = 2;% pixel separation between roi, deadzone, penumbra
% click2ROI2 is fixed size ROIs, independent of x-y profile 
%hROI = click2ROI2( maxDFoF0, radius, offset ); % handles contains info on ROIs
hROI = click2ROI2( maxStack, radius, offset ); % handles contains info on ROIs

nROIs = size( hROI, 2 ); % number of ROIs
%% get ROIs from user composite!
disp('PLEASE DO NOT CLOSE FIGURE! max 20 ROIs')
radius = 2;% pixels to search for half amplitude
offset = 1;% pixel separation between roi, deadzone, penumbra
% click2ROI2 is fixed size ROIs, independent of x-y profile 
hROI = click2ROI3( maxStack, maxDFoF0, radius, offset ); % handles contains info on ROIs
nROIs = size( hROI, 2 ); % number of ROIs
%% code for editing ROIs

%% MOVIE TIME! for adding manual ROI
% trying to use movie for generating manual ROI over targeted cell
% trying to add existing ROIs superimposed on movie
% get coords from movie frame and add to ROI handles
% % DOES NOT WORK!
% disp('make a movie')
% movfilename = baseName; % append(baseName,".avi"); % ext is auto '.mj2'
% % V = VideoWriter(movfilename,'Archival');
% % open(V)
% posPoly = hROI(1).ROI;
% mov = movieTimeBurnIn( baseDFoF0, posPoly );
% implay( mov, 100 );
% %% writeVideo( V, mov )
% 
% manROIhandle = click2ROIman( gca );
% % writeVideo( V, mov )
% % close(V)
% % eight clicks around perimeter
% 
% disp( 'done collecting ROIs')
%% start here with old data sets


%% loop over ROIs to create time course data with pen subtraction
%treatStats = roi2stats3D( treat, hROI ); % uses "raw" (cropped, smoothed etc)
disp('starting roi processing')
baseStats = roi2stats3D( base, hROI ); % uses "raw" (cropped, smoothed etc)
baseStats.timing = squeeze(timing);
disp('done with ROI stats')
% process ROI time course data for dF/F0
%  appends dF/F0 data to baseStats structure
disp('starting roi dFoF0')
baseStats = processROIs( baseStats );
%displayROIs( baseStats );
disp('done with roi dFoF0')

% EXPORT TO IGOR and MATLAB
fname = append( baseName, '_Igor');
export2igor( baseStats, fname ) % custom function for export ROI data
% save entire base stats structure
fname = append( baseName, '_baseStats');
save(fname, 'baseStats')
disp('export complete')

% %%
% for i=1:nROIs
%     displayIndyROIs( i, baseStats );
% end

% saves entire workspace, big file 
filename = append( baseName, 'complete.mat');
save(filename,'-v7.3')
disp('export complete complete')