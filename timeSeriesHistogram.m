function [ bins, h ] = timeSeriesHistogram( timeseries, binsize, xs, xe )
h=0;
%copied from https://www.mathworks.com/matlabcentral/answers/471182-histogram-with-time-data#answer_383553
noOfBins = xs : binsize : xe; % min(xs) : binsize : max(xe);
%plot histogram
%figure
h = histogram( timeseries, numel( noOfBins ) );
str=append('count ',num2str(binsize), ' seconds');
ylabel(str)
xlim([xs,xe])
bins = h.Values;
end