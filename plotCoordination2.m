function plotCoordination2( times , centertimes )
% times is an array, each row is a coordinated event, CE
% each column is the time of a "test" event in thate coordinated event
% each row has the "test" event time first,
% followed by at least one event from another ROI within maxDelta 

% get the starts and ends for each CE
nCE = size( times, 1 );
starts = min( times, [], 2);
ends = max( times, [], 2 );
%nEdges = nCE * 2 + 1;
%edges = zeros( nEdges,1 );
counts = zeros( nCE,1 );
widths = zeros( nCE, 1 );
centers = zeros( nCE, 1 );
% first edge is always zero
% first count is always zero

for k = 1 : nCE
    centers(k) = centertimes( k );
    widths(k) = ends(k) - starts(k);
    counts(k) = sum( ~isnan( times( k, : ) ) );
end
disp(append( 'nevents plotCoord2: ', num2str(sum(counts)) ))
hold on
for k=1:nCE
    bar(centers(k),counts(k),'BarWidth',widths(k));
end
xlabel('time (sec)' ); %,'FontSize',16)
ylabel( 'number of events' ); %,'FontSize',16)
title(append('brute force coordination') ); %,'FontSize',16)
hold off
end