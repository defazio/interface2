function [ spikeTimes, spikeDur, spikeAmp, trials, groups ] = prepRaster( analytes )

nFiles = size( analytes.File, 2 );
% width vs height
% concatenate all data, define "trials" with ROI, define "groups" with file
for iFile = 1 : nFiles
    nROI = size(analytes.File(iFile).ROI,2);
    for iROI = 1 : nROI
        times = analytes.File(iFile).ROI(iROI).times;
        nEvents = max( size( locs ) );
        sT = seconds( zeros(nEvents,1) );
        sD = zeros(nEvents,1);
        sA = zeros(nEvents,1);
        t = zeros(nEvents,1);
        g = zeros(nEvents,1);
        for iEvent = 1 : nEvents
            sT(iEvent) = seconds( times(iEvent) ); % append spike times
            sD(iEvent) = analytes.File(iFile).ROI(iROI).width(iEvent);
            sA(iEvent) = analytes.File(iFile).ROI(iROI).prominence(iEvent);
            t(iEvent) = iROI;
            g(iEvent) = iFile;
        end
        if iFile==1 && iROI==1
            spikeTimes = sT;
            spikeDur = sD;
            spikeAmp = sA;
            trials = t;
            groups = g;
        else
            spikeTimes = vertcat( spikeTimes, sT );
            spikeDur = vertcat( spikeDur, sD ) ;
            spikeAmp = vertcat( spikeAmp, sA );
            trials = vertcat( trials, t );
            groups = vertcat( groups, g );
        end
    end
end

end