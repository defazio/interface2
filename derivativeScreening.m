function [out, derivativeMax] = derivativeScreening(A, threshold)
%DERIVATIVESCREENING Filter out the points with large derivative values
% threshold = 137;
[n,m,k] = size(A);

derivativeMax = zeros(n,m);

% For each pixel point, the maximum value of the derivative over the time period
for i = 1:n
    for j = 1:m
          derivativeMax(i,j) = max(abs(diff(A(i,j,:))));
    end
end
out = derivativeMax>threshold;
end

