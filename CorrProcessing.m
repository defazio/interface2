function corrLagStore = CorrProcessing( ROIdataStruct, Fs, window_time ) %, maxlag_time, frac_overlap )

    %corr params SET THESE APPROPRIATELY!!
    % window_time = 1; % seconds
    % maxlag_time = 2; % seconds
    % frac_overlap = 0.95; % overlap fraction
    
    nFiles = size( ROIdataStruct, 2 );
    for iFile=1:nFiles
    
        arr = ROIdataStruct( iFile ).data; %.pensubdf; % ntimepoints x nROIs
    
        [ C, rrmean ] = ...
            corrRunner( arr, Fs, window_time ); %, maxlag_time, frac_overlap );

        corrLagStore(iFile).name = ROIdataStruct(iFile).name;
        corrLagStore(iFile).C = C;
        corrLagStore(iFile).L = NaN;
        corrLagStore(iFile).t = NaN;
        corrLagStore(iFile).Cmaxmean = rrmean;
        corrLagStore(iFile).Cbestlag = NaN;
    
    end
end
