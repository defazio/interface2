% REPLACES ROIPROCCESSING.MLX!!!

%STEP 3: after selecting ROIs, now detect events and correlate (can process all files in localpath)
%Detailed explanation of this function.

% select file
% this should be by file, dx should be consistent, but xs,xe changes?

localpath = '/ROIdataLT/*_Igor.mat';
ROIdataStruct = loadROIdata2(localpath);
set(0, 'DefaultAxesFontSize', 18);
set(groot,'defaultLineMarkerSize',10);
nFiles = size( ROIdataStruct, 2 );

%%

% all values in seconds
smooth = 1; % 7 works
derPreSmooth = 1;
derPostSmooth = 1; % 10 for visibility
% find peaks

% need to handle timing individually??

%Fs = 1/dx; % Hz, sampling frequency %% seconds %/60seconds = minutes
roll = 10; % seconds

prcthresh = 10;
Pfactor = 10;
% settings for peak detection
%minHeight = Pfactor * P
minwidth = 1;% / dx; % time in seconds / dx to get width in points
%widthRef = 'halfprom'; %'halfheight'; %
%minPeakProm = minHeight; % Pfactor * P; % dF/F0 units

analytes = analyzeROIs2( ROIdataStruct, roll, minwidth, smooth, ...
    Pfactor, prcthresh );
disp('completed event analysis...')

% combine all events for each file
events = linearize( analytes );
%%

% old style display
binsize = 10; % seconds

%bindata = NaN(nFiles,nbins);
ymin = -0.02; %min(ydata,[],"all");
ymax = 0.05; %max(ydata,[],"all");

%iFile = 9;
for iFile=1:nFiles
    xend = max(ROIdataStruct(iFile).xdata);
    xstart = 0;
    dx = mean(diff(ROIdataStruct(iFile).xdata)); % seconds
    Fs = 1/dx; % Hz
    %nbins = round(1 + (xend - xstart) / binsize );


% stack
    %figure
    sh = stackplots2(iFile, ROIdataStruct, ymin, ymax );
    figure
% raster 1
    tiledlayout(3,1)
    nexttile
    nofigure =1;
    hTDR = TDrasterIndex(analytes,iFile,nofigure);
    xlim([0 xend])
% time histo 2
    nexttile
    ts = events(iFile,:);
    %[bindata(iFile,:)] = timeSeriesHistogram( ts, binsize, xstart, xend );
    timeSeriesHistogram( ts, binsize, xstart, xend );
% movcorr 3   
    nexttile
    arr = ROIdataStruct(iFile).data;%.pensubdf;
    xdata = ROIdataStruct(iFile).xdata;
    %rotArr = rot90(arr);
    k = 50;
    %[rr,pp] = wrapperMovCorr(arr, k);
    %function [ rr, ll, t ] = wrapperCorrgram( xx, k, Fs, maxlag, frac_overlap )
    maxlag_time = 5; % seconds
    maxlag = maxlag_time * Fs;
    frac_overlap = 0.5; 
    
    [ rr, ll ] = wrapperMovCorr( arr, k ); %, Fs, maxlag, frac_overlap );
%    [ rr, ll, t ] = wrapperCorrgram( arr, k, Fs, maxlag, frac_overlap );
%    [ rr, ll, t ] = wrapperMIgram( arr, k, Fs, maxlag, frac_overlap );
%    [ rr, ll, t ] = wrapperTWX( arr, k, Fs, maxlag, frac_overlap );


 %   plotCorrLag( xdata, arr, rr, ll, t, Fs );
    meanrr = mean( rr, 2 );
    str = append('iFile: ',num2str(iFile),' ',ROIdataStruct(iFile).name);
    %h = figure('Name',num2str(iFile)); 
    plot(xdata, meanrr);
    title(str);
    ylim([-0.1 1])
    xlim([0 xend])
end

% run plot corr lag script
% cannot run in live script mode!!
% plotCorrLagScript

